#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/12 15:12:42
'''
import sys
sys.path.append('.')

from db_logic.db_base import Repository

from models.medicament_models import EntityMedicamentExtend

from Common.Utils import Utils


#药剂品种业务逻辑类
class BllMedicamentExtend(Repository):
    def __init__(self, entityType=EntityMedicamentExtend):
        super().__init__(entityType)


    def get_list(self, func_type, page_param):
        sql_all = f"""
            select id, name, description,key_lenth, sort_index, is_use from rms_medicament_extend where func_type='{func_type}' and is_del=0 order by sort_index
        """
        try:
            count_number = len(self.execute(sql_all).fetchall())
        except Exception:
            count_number = 0
        
        page_param.totalRecords=count_number
        page_sql = Utils.sql_paging_assemble(sql_all, page_param)
        return self.execute(page_sql).fetchall()
    
    def get_use_list(self, func_type):
        sql_all = f"""
            select id, name, description,key_lenth, sort_index, is_use from rms_medicament_extend where func_type='{func_type}' and is_del=0 and is_use=1 order by sort_index
        """
        return self.execute(sql_all).fetchall()

    def random_inster_func_type(self):
        import random
        data_list = self.findList().all()
        for i in data_list:
            setattr(i, "func_type", random.randint(1,4))
            self.update(i)