#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/25 09:38:05
'''
import sys

sys.path.append(".")

from Common.Utils import Utils
from sqlalchemy import desc, Table

from db_logic.db_base import Repository
from models.medicament_models import EntityMedicamentTemplate


#药剂品种业务逻辑类
class BllMedicamentTemplate(Repository):

    def __init__(self, entityType=EntityMedicamentTemplate):
        return super().__init__(entityType)

    # 获取所有模板列表
    def getAllTemplateList(self, client_id, func_type, page_param):
        filter_base = ""
        if client_id:
            filter_base += f" client_id='{client_id}' "
        if func_type:
            if filter_base:
                filter_base += " and "
            filter_base += f" func_type='{func_type}' "
        if filter_base:
            filter_base = f" where {filter_base} "
        sql_all = f"select * from rms_medicament_template {filter_base} order by create_date desc"
        try:
            count_number = len(self.execute(sql_all).fetchall())
        except Exception:
            count_number = 0
        page_param.totalRecords = count_number
        sql_all = Utils.sql_paging_assemble(sql_all, page_param)
        return self.execute(sql_all).fetchall()
    
    # 批量或删除单个模板
    def del_template_obj(self, tmp_list):
        data_list = self.findList(EntityMedicamentTemplate.template_id.in_(tuple(tmp_list))).all()
        for i in data_list:
            self.session.delete(i)
        self.session.commit()
        # sql_all = f"DELETE from rms_medicament_template WHERE template_id in {tuple(tmp_list)}"

        # return self.execute(sql_all)


    def inster_base_log(self, asss):
        import random, json
        li_ba = []
        for i in range(1, 5):
            obj = EntityMedicamentTemplate(
                client_id="1c39cb24-07f8-11ed-abd4-f47b094925e1",
                client_name="测试机器",
                template_name=f"测试模板{i}",
                template_content=json.dumps(asss),
                is_wait_export=1,
                start_bar_code=random.randint(100001, 100111),
                bar_code_count=len(asss),
                create_date=Utils.get_str_datetime(),
                create_user_id="c0858e96-d900-11eb-8209-009027e3906b",
                create_user_name="admin",
            )
            li_ba.append(obj)
        self.insert_many(li_ba)


if __name__ == '__main__':
    # import json
    # data = BllMedicamentTemplate().getAllTemplateList("")
    # # data = Utils.resultAlchemyData(json.loads(data))
    

    # # data =BllMedicamentTemplate().entityType.to_dict(data_list=data)
    # print(data)
    asss = [
        {"variety_id": "f8442a6e-9950-11ec-a502-e2052a1045e4", "remark1": "", "remark2": "", "remark3": "\u6613\u5236\u6bd2", "remark4": "", "name": "\u7532\u82ef", "english_name": "", "purity": "AR\uff08\u6caa\u8bd5\uff09\uff0c\u2265 99.5%", "cas_number": "108-88-3", "remark5": "",
            "speci": "500", "speci_unit": "ml", "export_count": "1", "remark6": "\u74f6", "production_date": "2022-03-01", "shelf_life": 10529, "price": "0", "is_supervise": 0, "remain": "683", "manufacturer": "\u56fd\u836f", "remark8": "", "remark9": "\u70f7\u57fa\u6c5e", "remark10": ""},
        {"variety_id": "f8442a6f-9950-11ec-9253-e2052a1045e4", "remark1": "", "remark2": "", "remark3": "\u5371\u5316\u54c1", "remark4": "", "name": "\u82ef\u915a", "english_name": "", "purity": "AR\uff08\u6caa\u8bd5\uff09\uff0c\u2265 99.0%", "cas_number": "108-95-2", "remark5": "",
            "speci": "500", "speci_unit": "ml", "export_count": "1", "remark6": "\u74f6", "production_date": "2022-03-01", "shelf_life": 10529, "price": "0", "is_supervise": 0, "remain": "848", "manufacturer": "\u56fd\u836f", "remark8": "", "remark9": "\u6325\u53d1\u915a", "remark10": ""},
        {"variety_id": "f8442a70-9950-11ec-a157-e2052a1045e4", "remark1": "", "remark2": "", "remark3": "\u5371\u5316\u54c1", "remark4": "", "name": "\u82ef", "english_name": "", "purity": "AR\uff08\u6caa\u8bd5\uff09\uff0c\u2265 99.5%", "cas_number": "71-43-2", "remark5": "",
            "speci": "500", "speci_unit": "ml", "export_count": "1", "remark6": "\u74f6", "production_date": "2022-03-01", "shelf_life": 10529, "price": "0", "is_supervise": 0, "remain": "640", "manufacturer": "\u56fd\u836f", "remark8": "", "remark9": "\u6cb9", "remark10": ""},
        {"variety_id": "f8442a71-9950-11ec-9894-e2052a1045e4", "remark1": "", "remark2": "", "remark3": "\u5371\u5316\u54c1", "remark4": "", "name": "\u4e59\u9178\uff0836%\uff09", "english_name": "", "purity": "AR", "cas_number": "64-19-7", "remark5": "", "speci": "500",
            "speci_unit": "ml", "export_count": "1", "remark6": "\u74f6", "production_date": "2022-03-01", "shelf_life": 10529, "price": "0", "is_supervise": 0, "remain": "734", "manufacturer": "\u56fd\u836f", "remark8": "", "remark9": "\u70f7\u57fa\u6c5e", "remark10": ""},
        {"variety_id": "f84451a6-9950-11ec-8925-e2052a1045e4", "remark1": "", "remark2": "", "remark3": "\u5371\u5316\u54c1", "remark4": "", "name": "\u4e59\u9178\u9150", "english_name": "", "purity": "AR", "cas_number": "106-24-7", "remark5": "", "speci": "500",
            "speci_unit": "ml", "export_count": "1", "remark6": "\u74f6", "production_date": "2022-03-01", "shelf_life": 10529, "price": "0", "is_supervise": 0, "remain": "749", "manufacturer": "\u56fd\u836f", "remark8": "", "remark9": "\u70f7\u57fa\u6c5e", "remark10": ""},
        {"variety_id": "f84451a7-9950-11ec-9b4e-e2052a1045e4", "remark1": "", "remark2": "", "remark3": "\u5371\u5316\u54c1", "remark4": "", "name": "\u56db\u6c2f\u4e59\u70ef", "english_name": "", "purity": "\u73af\u4fdd\u7ea7", "cas_number": "127-18-4", "remark5": "", "speci": "500",
            "speci_unit": "ml", "export_count": "28", "remark6": "\u74f6", "production_date": "2022-03-01", "shelf_life": 10529, "price": "0", "is_supervise": 0, "remain": "1207", "manufacturer": "\u5929\u6d25\u50b2\u7136", "remark8": "", "remark9": "\u6cb9", "remark10": ""},
        {"variety_id": "f84451a8-9950-11ec-96ca-e2052a1045e4", "remark1": "", "remark2": "", "remark3": "\u5371\u5316\u54c1", "remark4": "", "name": "\u56db\u6c2f\u5316\u78b3", "english_name": "", "purity": "\u7ea2\u5916\u6d4b\u6cb9\u4eea\u4e13\u7528", "cas_number": "56-23-5", "remark5": "", "speci": "500", "speci_unit": "ml", "export_count": "31", "remark6": "\u74f6", "production_date": "2022-03-01", "shelf_life": 10529, "price": "0", "is_supervise": 0, "remain": "1126", "manufacturer": "\u5b89\u8c31", "remark8": "", "remark9": "\u77ff\u7269\u6cb9", "remark10": ""}]
    
    rsp = BllMedicamentTemplate().inster_base_log(asss)
    print(rsp)