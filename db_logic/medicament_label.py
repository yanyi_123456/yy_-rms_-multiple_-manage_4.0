#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/09/09 16:22:25
'''
import sys
sys.path.append('.')

from db_logic.db_base import Repository

from models.medicament_models import EntityMedicamentLabel

from Common.Utils import Utils


class BllMedicamentLabel(Repository):
    def __init__(self, entityType=EntityMedicamentLabel):
        return super().__init__(entityType)

    def get_attribute_label_list_info(self):
        sql_all = """
            SELECT DISTINCT description FROM `rms_medicament_label`
        """
        return self.execute(sql_all).fetchall()


    def get_seach_list(self, seach_word, page_param):
        filter_base = ""
        if seach_word:
            seach_word = f"%{seach_word}%"
            filter_base += f" name like '{seach_word}' "

        if filter_base:
            filter_base = f" where {filter_base}"
        sql_all = f"""
            select * from rms_medicament_label {filter_base}
        """
        try:
            count_number = len(self.execute(sql_all).fetchall())

        except:
            count_number = 0
        page_param.totalRecords = count_number
        page_sql = Utils.sql_paging_assemble(sql_all, page_param)
        return self.execute(page_sql).fetchall()
