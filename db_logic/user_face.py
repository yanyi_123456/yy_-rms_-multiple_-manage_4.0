#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/27 14:51:13
'''
import sys
sys.path.append('.')
from db_logic.db_base import Repository
from models.user_models import EntityUserFace


class BllUserFace(Repository):

    def __init__(self, entityType=EntityUserFace):
        return super().__init__(entityType)
