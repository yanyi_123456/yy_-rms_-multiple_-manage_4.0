#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/08 15:24:09
'''
import sys
sys.path.append('.')

from db_logic.db_base import Repository
from models.msds_dangerous_models import EntityDangerous
from Common.Utils import Utils
#用户操作业务逻辑类


class BllDangerous(Repository):

    def __init__(self, entityType=EntityDangerous):
        super().__init__(entityType)

    def get_seach_list(self, seach_word, page_param):
        filter_base = ""
        if seach_word:
            seach_word = f"'%{seach_word}%'"
            filter_base += f" name like {seach_word} or english_name like {seach_word} "
        if filter_base:
            filter_base = f" where {filter_base} "
        sql_all = f"""
            select * from rms_dangerous {filter_base}
        """
        try:
            count_number = len(self.execute(sql_all).fetchall())
        except Exception:
            count_number = 0
        page_param.totalRecords = count_number
        page_sql = Utils.sql_paging_assemble(sql_all, page_param)
        return self.execute(page_sql).fetchall()
