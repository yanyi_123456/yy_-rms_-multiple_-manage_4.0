#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/09 17:41:46
'''
import sys
sys.path.append('.')

from db_logic.db_base import Repository
from models.client_models import EntityClientCellUser

#用户操作业务逻辑类
class BllClientCellUser(Repository):

    def __init__(self, entityType=EntityClientCellUser):
        super().__init__(entityType)

