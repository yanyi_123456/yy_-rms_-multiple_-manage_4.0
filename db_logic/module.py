#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/01 15:36:46
'''
import sys
sys.path.append('.')

from db_logic.db_base import Repository
from models.power_models import EntityModule
from Common.Utils import Utils

class BllModule(Repository):

    def __init__(self, entityType=EntityModule):
        return super().__init__(entityType)

    def get_module_list(self):
        sql_all = """
            select module_id, module_type, module_name, sort_index, description from rms_module where is_enabled=1
        """
        # data = {
            
        #     # "client_list":[]
        #     "client_manage": [],
        #     "sys_manage": [],
        #     "standard_manage": [],
        #     "consumables_manage": [],
        #     "instrument_manage": []
        # }
        # sql_data = Utils.msyql_table_model(self.execute(sql_all).fetchall())
        # for i in sql_data:
        #     if i["module_type"] == "1":
        #         data["client_manage"].append(i)
        #     elif i["module_type"] == "2":
        #         data["drug_manage"].append(i)
        #     elif i["module_type"] == "3":
        #         data["standard_manage"].append(i)
        #     elif i["module_type"] == "4":
        #         data["consumables_manage"].append(i)
        #     elif i["module_type"] == "5":
        #         data["instrument_manage"].append(i)
        # return data
        sql_data = Utils.msyql_table_model(self.execute(sql_all).fetchall())
        data_dict = {
            "client_manage":[],
            "drug_manage": [],
            "standard_manage": [],
            "consumables_manage": [],
            "instrument_manage": []
        }
        for i in sql_data:
            if i["module_type"] == "1":
                data_dict["client_manage"].append(i)
            elif i["module_type"] == "2":
                data_dict["drug_manage"].append(i)
            elif i["module_type"] == "3":
                data_dict["standard_manage"].append(i)
            elif i["module_type"] == "4":
                data_dict["consumables_manage"].append(i)
            elif i["module_type"] == "5":
                data_dict["instrument_manage"].append(i)

        return data_dict


if __name__ == '__main__':
    from Common.Utils import Utils
    aaa = BllModule().get_module_list()
    # print([i for i in aaa])
    aaa_list = Utils.to_dict(aaa, 1)
    for i in aaa_list:
        print(i)
    print(aaa)
