#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/01 17:14:39
'''

import sys
sys.path.append('.')

from db_logic.db_base import Repository
from models.user_models import EntityRole

# 角色类
class BllRole(Repository):

    def __init__(self, entityType=EntityRole):
        return super().__init__(entityType)

    def add_inster_role_user(self):
        obj_list = []
        name_list = [
            {"code": "admin", "name": "管理员", "index": 2},
            {"code": "maintain_user", "name": "维护人员", "index": 3},
            {"code": "simple_user", "name": "普通用户", "index": 1},
            
        ]
        for i in range(3):
            obj = EntityRole(
                role_code=name_list[i].get("code"),
                role_name=name_list[i].get("name"),
                role_level=name_list[i].get("index"),
                sort_index=name_list[i].get("index"),
                is_enabled=1

            )
            obj_list.append(obj)
        self.insert_many(obj_list)

if __name__ == '__main__':
    BllRole().add_inster_role_user()