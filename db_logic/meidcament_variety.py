#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/19 09:43:08
'''
import sys
sys.path.append(".")
from sqlalchemy import or_, and_
from Common.Utils import Utils
from db_logic.db_base import Repository
from models.medicament_models import EntityMedicamentVariety


#药剂品种业务逻辑类
class BllMedicamentVariety(Repository):
    #_instance_lock = threading.Lock()
    ## 实现单例模式
    #def __new__(cls, *args, **kwargs):
    #    if not hasattr(BllMedicamentVariety, "_instance"):
    #        with BllMedicamentVariety._instance_lock:
    #            if not hasattr(BllMedicamentVariety, "_instance"):
    #                BllMedicamentVariety._instance = object.__new__(cls)
    #    return BllMedicamentVariety._instance

    def __init__(self, entityType=EntityMedicamentVariety):
        return super().__init__(entityType)

    # 创建药剂品种
    def createDrugVariety(self, customer_id, drug_info, user_info):
        entity = self.findEntity(
            and_(
                EntityMedicamentVariety.name == drug_info.get("name"),
                EntityMedicamentVariety.purity == drug_info.get("purity"),
                EntityMedicamentVariety.net_weight_unit == drug_info.get("net_weight_unit"),
                EntityMedicamentVariety.net_weight == drug_info.get("net_weight")
            )
        )
        if not entity:
            entity = EntityMedicamentVariety(
                customer_id=customer_id,
                name=drug_info.get("name"),
                english_name=drug_info.get("english_name"),
                cas_number=drug_info.get("cas_number"),
                purity=drug_info.get("purity"),
                tp=drug_info.get("tp", 1),
                net_weight_unit=drug_info.get("net_weight_unit"),
                net_weight=drug_info.get("net_weight"),
                create_date=Utils.get_str_datetime(),
                create_user_id=user_info.user_id,
                create_user_name=user_info.real_name,
                shelf_life_warning_value=10,
                inventory_warning_value=10,
                use_days_warning_value=10,
                total_count=1,
                normal_count=1,
                use_count=0,
                empty_count=0,
                is_supervise=0
            )
            self.insert(entity)
        else:
            entity.total_count += 1
            entity.normal_count += 1
            self.update(entity)
        entity = self.session.merge(entity)
        return entity
    # # 获取品种统计
    # def getVarietyStatistics(self):
    #     SQL = """
    #             SELECT  b.VarietyId as VarietyId, COUNT(CASE when IsEmpty = 1 then 1 end) as QuarterlyEmptyCount, 
    #                   sum(CASE when IsEmpty = 1 then a.price else 0 end) as QuarterlyEmptyPrice, COUNT(CASE when RecordType = 1 then 1 end) as 
    #                   QuarterlyPutInCount from RMS_MedicamentVariety  as b LEFT JOIN RMS_MedicamentRecord as a on
    #                    b.VarietyId = a.VarietyId and MONTH((a.CreateDate)) = MONTH(now()) and Date_format
    #                   (a.CreateDate, '%Y') = (DATE_FORMAT(now(), '%Y')) GROUP by b.VarietyId
    #             """
    #     # 获取季度数据
    #     quarter_data = BllMedicamentVariety().execute(SQL).fetchall()
    #     quarter_data = json.loads(Utils.resultAlchemyData(
    #         Utils.mysqlTable2Model(quarter_data)))
    #     SQL = """SELECT  b.VarietyId as VarietyId, COUNT(CASE when IsEmpty = 1 then a.CreateDate end) 
    #                   as YearEmptyCount, sum(CASE when IsEmpty = 1 then a.price else 0 end) as YearEmptyPrice,
    #                   COUNT(CASE when RecordType = 1 then 1 end) as YearPutInCount from RMS_MedicamentVariety  as b LEFT JOIN 
    #                   RMS_MedicamentRecord as a on b.VarietyId = a.VarietyId and year((a.CreateDate)) = 
    #                   year(now()) GROUP by b.VarietyId"""

    #     # 获取年度数据
    #     year_data = BllMedicamentVariety().execute(SQL).fetchall()
    #     year_data = json.loads(Utils.resultAlchemyData(
    #         Utils.mysqlTable2Model(year_data)))
    #     SQL = """
    #                select  a.VarietyId as VarietyId, a.Name,a.Purity,a.CASNumber,a.IsSupervise, 
    #                sum(case when b.`Status` = 1 then 1 else 0 end) as NormalCount,  
    #                sum(case when b.`Status` = 1 or b.`Status` = 2 then 1 else 0 end) as TotalCount,
    #                sum(case when b.`Status` = 2 then 1 else 0 end) as UseCount, 
    #                sum(case when b.`Status` = 1 or b.`Status` = 2 then b.Price else 0 end)  as StockPrice 
    #                from RMS_MedicamentVariety as a LEFT JOIN RMS_Medicament
    #                as b on b.VarietyId = a.VarietyId GROUP BY a.VarietyId
    #            """
    #     # 获取药剂数据
    #     med_data = BllMedicamentVariety().execute(SQL).fetchall()
    #     med_data = json.loads(Utils.resultAlchemyData(
    #         Utils.mysqlTable2Model(med_data)))
    #     data_list = []
    #     new_data_list = []
    #     # 便利数据合通过VarietyId相同合并字典
    #     for quarter in quarter_data:
    #         for year in year_data:
    #             if quarter['VarietyId'] == year['VarietyId']:
    #                 new_data = dict(quarter, **year)
    #                 data_list.append(new_data)
    #     for data_dict in data_list:
    #         for med in med_data:
    #             if data_dict['VarietyId'] == med['VarietyId']:
    #                 new_data = dict(data_dict, **med)
    #                 new_data_list.append(new_data)
    #     return new_data_list
#     def inster_type_drug(self):
#         name_list = ["氧化钠", "砷", "硫酸"]
#         for i in name_list:
#             obj = EntityMedicamentVariety(
#                 name=i,
#                 purity='国标',
#                 inventory_warning_value=10,
#                 shelf_life_warning_value=10,
#                 use_days_warning_value=10,
#                 empty_count=0,
#                 use_count=0,
#                 normal_count=10,
#                 total_count=10,
#                 create_date=Utils.get_str_datetime(),
#                 create_user_id='4cea74ee-0d8b-11ed-943e-f47b094925e1'
#             )
#             self.insert(obj)

# if __name__ == '__main__':
#     BllMedicamentVariety().inster_type_drug()