#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/09 17:38:47
'''
import sys
sys.path.append('.')

from db_logic.db_base import Repository
from models.client_models import EntityClientCell
from Common.Utils import Utils

class BllClientCell(Repository):

    def __init__(self, entityType=EntityClientCell):
        super().__init__(entityType)

    # 展示抽屉权限用户
    def get_drawer_power_user_list(self, drawer_id, client_id, page_param):
        sql_all = f"""
        select a.real_name, a.user_id,a.user_code,
            CASE WHEN b.id is not null THEN 0 ELSE 1 END drawer_type 
        from rms_user as a LEFT JOIN (
            select * from rms_client_cell_user where client_cell_id='{drawer_id}' and client_id='{client_id}'
        ) as b on a.user_id=b.user_id
        group by user_id
        """
        try:
            count_number = len(self.execute(sql_all).fetchall())
        except Exception:
            count_number = 0
        page_param.totalRecords = count_number
        page_sql = Utils.sql_paging_assemble(sql_all, page_param)
        return self.execute(page_sql).fetchall()
    
    # 插入测试数据
    def inster_test_data(self):
        client_id = '1c39cb24-07f8-11ed-abd4-f47b094925e1'
        obj_list = []
        for i in range(1,9):
            obj_list.append(
                EntityClientCell(
                    cell_code=i,
                    client_id=client_id,
                    cell_speci=i

                )
            )
        self.insert_many(obj_list)


if __name__ == '__main__':
    BllClientCell().inster_test_data()