#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Da
te:2022/07/18 16:32:26
'''
import sys
sys.path.append(".")
from Common.Utils import Utils

import os
from sqlalchemy import asc
from db_logic.db_base import Repository
from models.client_models import EntityClient



#用户操作业务逻辑类
class BllClient(Repository):
    def __init__(self, entityType=EntityClient):
        return super().__init__(entityType)


    # 获取客户端列表
    def get_all_client_list(self, func_type):
        # return self.findList().order_by(asc(EntityClient.client_code)).all()
        sql_all = f"""select * from rms_client where func_type='{func_type}' order by client_code """
        return self.execute(sql_all).fetchall()


    def get_client_seach(self, seach_word, func_type, page_param):
        filter_base = ""
        if seach_word:
            filter_base += f" client_name like '%{seach_word}%' "
        # 添加类别参数
        if func_type:
            if filter_base:
                filter_base += " and "
            filter_base += f" func_type='{func_type}' "
        if filter_base:
            filter_base = f" where {filter_base} "
        sql_all = f"""
            select * from rms_client {filter_base} order by client_code
        """
        try:
            count_number = self.execute(f"select count(*) num from rms_client {filter_base} order by client_code").fetchone().num
        except Exception:
            count_number = 0
        page_param.totalRecords=count_number

        page_sql = Utils.sql_paging_assemble(sql_all, page_param)
        return self.execute(page_sql)

    # 根据条件查询客户端信息
    def get_filter_client(self, client_id, customer_id):
        where_list = []
        if client_id:
            where_list.append(EntityClient.client_id == client_id)
        if customer_id:
            where_list.append(EntityClient.customer_id == customer_id)
        return self.findEntity(tuple(where_list))

    def empty_table_info(self):
        # 清空所有需删除表数据
        # rms_user_medicament, rms_medicament_variety,
        # rms_medicament_template,rms_medicament_record,
        # rms_medicament_extend,rms_medicament, rms_humiture_record
        # 清空指定柜体需删除
        # rms_medicament_template,rms_medicament_record,rms_medicament, rms_humiture_record
        table_list = [
            "rms_user_medicament", "rms_medicament_variety",
            "rms_medicament_template","rms_medicament_record",
            "rms_medicament_extend","rms_medicament", "rms_humiture_record"
        ]
        for i in table_list:
            sql_all = f"truncate {i}"
            self.executeNoParam(sql_all)
        return True
