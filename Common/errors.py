#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/27 16:11:57
'''
from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES

from Common.Utils import Utils

def error_response(status_code, message=None):
    payload = Utils.except_return(
        msg="请登录", status=status_code)
    # payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['msg'] = message
    response = jsonify(payload)
    response.status_code = 200
    return response