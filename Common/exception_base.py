#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:14:51
'''
import json
from werkzeug.exceptions import HTTPException

class APIException(HTTPException):

    code = -1
    msg = "请求错误"
    data = []

    def __init__(self, code=None, msg=None, data=None, headers=None):
        if code:
            self.code = code
        if msg:
            self.msg = msg
        if data:
            self.data = data
        super(APIException, self).__init__(msg, None)

    def get_body(self, environ=None, scope=None):
        body = dict(
            code=self.code,
            msg=self.msg,
            data=self.data
        )
        text = json.dumps(body)
        return text

    def get_headers(self, environ=None, scope=None):
        return [('Content-Type', 'application/json')]
