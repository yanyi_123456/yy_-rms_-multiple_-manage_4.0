"""
此文件是用来生成Code128条形码 并实现打印功能
"""
import os

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QApplication
from PyQt5.QtPrintSupport import QPrinter
from pystrich.code128 import Code128Encoder
import uuid


class CreateBarcode:

    """
        Code128Encoder(options={}) options参数
    * ttf_font：用于呈现标签的truetype字体文件的绝对路径
    * ttf_fontsize：绘制标签的字体大小
    * label_border：条形码和标签之间的像素空间数
    * bottom_border：标签和底部边框之间的像素空间数
    * height：图像的高度（以像素为单位）
    * show_label：是否在条形码下面显示标签（默认为True）“”
    """
    code_path = os.path.join(os.getcwd(), "img")
    if not os.path.exists(code_path):
        os.umask(0)
        os.makedirs(code_path)
    def __init__(self):
        pass
        
        
    def create_Code128_img(self, barcode):
        a = Code128Encoder(barcode, options={
            'ttf_font': os.path.join(os.getcwd(),'arial.ttf'),
            'label_border': 0, 'height': 45,  'bottom_border': 0,'ttf_fontsize':13,
        })
        # bar_width 条码宽度尺寸
        
        
        file_name = os.path.join(self.code_path, str(uuid.uuid4()) + '.png')
        a.save(file_name, bar_width=1)
        self.printer_code(file_name)

    def printer_code(self, file_name):
        a = QApplication([])
        document = QTextDocument()
        html = """
        <head>
        <title>Report</title>
        <style>
        </style>
        </head>
        <body>
        <img src="{}">
        </body>
        """.format(file_name)

        document.setHtml(html)
        printer = QPrinter()
        printer.setPageSize(QPagedPaintDevice.Custom)
        printer.setPaperSize(QSizeF(30.0,18.0),QPrinter.Millimeter)
        # 设置纸张到条码的边距  左上下右
        printer.setPageMargins(0, 3, 0, 0, QPrinter.Millimeter)

        #document.setPageSize(QSizeF(printer.pageRect().size()))
        document.setPageSize(QSizeF(100.0,65.0))
        
        print('正在打印中。。。。')
        document.print_(printer)
        print('打印完成。。')
        # os.remove(file_name)

    def create_drug_lobel_code(self, **kwargs):
        a = Code128Encoder(
            kwargs.get("code_number"),
            options={
            'ttf_font': os.path.join(os.getcwd(),'arial.ttf'),
            'label_border': 0, 'height': 15,  'bottom_border': 0,'ttf_fontsize':0,
        })
        # bar_width 条码宽度尺寸
        
        
        file_name = os.path.join(self.code_path, str(uuid.uuid4()) + '.png')
        a.save(file_name, bar_width=1)
        kwargs["file_path"] = file_name
        self.printer_drug_label(**kwargs)

    def printer_drug_label(self, **kwargs):
        a = QApplication([])
        document = QTextDocument()
        #TODO 添加溶剂
        html = """
        <head>
        <title>Report</title>
        <style></style>
        </head>
        <body>
        <div>
        <p style="font-size:12px;">
        <span style="font-size:13px;">珠海市食品药品检验所</span> <br />
        名称: {} <br />
        编号: {} <br />
        浓度: {} &nbsp;&nbsp;溶剂: {}<br />
        配置日期: {} <br />
        有 效 期：{} <br />
        配 置 人：{} <br />
        </p>
        <img src="{}">
        </div>
        </body>
        """.format(
            
            kwargs.get("name"),
            kwargs.get("code_number"),
            kwargs.get("purity"),
            kwargs.get("solvent"),
            kwargs.get("start_time")[:10],
            kwargs.get("end_time")[:10],
            kwargs.get("user_name"),
            kwargs.get("file_path"),
            )
        document.setHtml(html)
        printer = QPrinter()
        printer.setPageSize(QPagedPaintDevice.Custom)
        printer.setPaperSize(QSizeF(60.0,40.0),QPrinter.Millimeter)
        # printer.setPaperSize(QSizeF(30.0,50.0),QPrinter.Millimeter)
        # 设置纸张到条码的边距  左上下右
        printer.setPageMargins(6, 2, 0, 0, QPrinter.Millimeter)

        document.setPageSize(QSizeF(printer.pageRect().size()))
        #document.setPageSize(QSizeF(50.0,30.0))
        
        print('正在打印中。。。。')
        document.print_(printer)
        print('打印完成。。')
        # os.remove(file_name)


if __name__ == '__main__':
    kwS = {
        "code_number":"200001",
        "medicament_name":"1111111",
        "purity":"12",
        "solvent": "2313",
        "start_time": "2022-10-15",
        "end_time": "2022-10-16",
        "user_name":"测试账户"
    }
    CreateBarcode().create_drug_lobel_code(**kwS)
    # CreateBarcode().create_Code128_img("200000001")
