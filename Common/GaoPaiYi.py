import requests
import time
import json
import uuid
import sys

# 获取分辨率索引
url = "http://localhost:6543/GetAllDisplayInfo"
param={
    "dev_idx":"0",
}
#res = requests.get(url = url)
#print(json.loads(res.text))
#print(json.loads(res.text)['data'])
# print(json.loads(res.text)['code']=='0')


class GaoPaiYi():
    path  = "/home/yanyi/Project/MainManage"  # 默认存储地址
    def __init__(self):
        pass

    def getPic(self,path=''):
        try:
            if path:
                self.path = path
            #print('----------预览视频-------------------------')
            # 打開視頻
            url = "http://localhost:6543/StartPreview?dev_idx=0&res_id=0&pixfmt=pixfmt"
            res = requests.get(url = url)
            print(json.loads(res.text))
            #print(json.loads(res.text)['data'])
            # print(json.loads(res.text)['code']=='0')
            if json.loads(res.text)['returnCode']!=0:
                return  {'code':-1,'msg':"打开视频失败"}
            time.sleep(2)

            #print('----------拍照-------------------------',__file__)
            # 打開視頻
            url = "http://localhost:6543/getPic?savepath="+self.path+"&quality=80"
            res = requests.get(url = url)

            #print(json.loads(res.text)['data']["path"])
            #print(json.loads(res.text)['returnCode']==0) /home/yanyi/Project/MainManage/img/2022-9-9 15-36-35.jpg
            if json.loads(res.text)['returnCode']==0:
                res_data =   {'code':0,'msg':"图片保存成功","path":json.loads(res.text)['data']["path"]}
            else:
                res_data= {'code':-1, 'msg':"图片保存失败", "path":json.loads(res.text)['returnCode']}

            #print('----------关闭视频-------------------------')
            url = "http://localhost:6543/StopPreview?dev_idx=0"
            res = requests.get(url = url)

            return res_data

        except Exception as e :
            print('报错信息: ' + e + ' ,File: ' + __file__ + ', Line ' + str(sys._getframe().f_lineno))
            return {'code':-1,'msg':e}


if __name__ =="__main__":
    path= '/tmp'
    pic_url = GaoPaiYi().getPic(path)
    print(pic_url)
