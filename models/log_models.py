#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:30:14
'''
import sys
sys.path.append(".")
# 已同步本地数据库
from sqlalchemy import Column, String, Integer
from models.models_base import Base, get_uuid


class EntityLog(Base):
    __tablename__ = "rms_log"
    __table_args__ = (
        {
            "comment": "日志信息实体类"
        }
    )

    log_id = Column(String(50), primary_key=True, comment='日志ID', default=get_uuid)  # 日志ID
    customer_id = Column(String(50), comment='客户ID')  # 客户ID
    log_type = Column(String(50), comment='日志类型')  # 日志类型
    operate_user_id = Column(String(50), comment='操作用户ID')  # 操作用户ID
    operate_account = Column(String(50), comment='操作账户')  # 操作账户
    operate_user_name = Column(String(50), comment='操作用户名')  # 操作用户名
    operate_type_code = Column(String(50), comment='操作类型编号')  # 操作类型编号
    operate_type = Column(String(50), comment='操作类型')  # 操作类型
    ip_address = Column(String(50), comment='IP地址')  # IP地址
    execute_result_code = Column(String(50), comment='执行结果代码')  # 执行结果代码
    execute_result = Column(String(50), comment='执行结果')  # 执行结果
    operate_date = Column(String(50), comment='操作日期')  # 操作日期
    is_add = Column(Integer, comment='是否添加', default=0)
