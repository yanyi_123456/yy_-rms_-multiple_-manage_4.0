#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/28 08:37:59
'''
import sys
sys.path.append(".")
from sqlalchemy import Column, String, Integer
from models.models_base import Base, get_uuid


#终端模块实体类
class EntityModule(Base):
    __tablename__ = "rms_module"
    __table_args__ = (
        {
            "comment": "终端模块表"
        }
    )

    module_id = Column(String(50), primary_key=True, comment="模块ID", default=get_uuid)
    # 模块类型分析，1柜体，2试剂管理，3 标准品管理，4耗材管理，5仪器管理
    module_type = Column(String(50), comment="模块类型(1柜体,2后台)")
    module_code = Column(String(50), comment="")
    module_name = Column(String(50), comment="模块名称")
    icon = Column(String(50), comment="图片")
    url_address = Column(String(50), comment="图片地址")
    sort_index = Column(Integer, comment="排序下标")
    module_weight = Column(Integer, comment="模块权重")
    parent_id = Column(String(50), comment="父id")
    is_enabled = Column(Integer, comment="是否启用")
    description = Column(String(50), comment="备注")
    create_date = Column(String(50), comment="创建时间")
    create_user_id = Column(String(50), comment="创建用户id")
    create_user_name = Column(String(50), comment="创建用户名")
    is_add = Column(Integer, comment="", default=0)



# #终端模块权限实体类
class EntityModuleRelation(Base):
    __tablename__ = "rms_module_relation"
    __table_args__ = (
        {
            "comment": "模块权限表"
        }
    )

    module_relation_id = Column(String(50), primary_key=True, comment="模块权限id", default=get_uuid)
    customer_id = Column(String(50), comment="客户id")
    object_type = Column(String(50), comment="类型")
    object_id = Column(String(50), comment="类型id")
    # 模块类型分析，1柜体，2试剂管理，3 标准品管理，4耗材管理，5仪器管理
    module_type = Column(String(50), comment="模块类型(1柜体,2后台)")
    module_id = Column(String(50), comment="模块id")
    create_date = Column(String(50), comment="创建时间")
    create_user_id = Column(String(50), comment="创建用户id")
    create_user_name = Column(String(50), comment="创建用户名称")
    is_add = Column(Integer, comment="", default=0)

# if __name__ == '__main__':
#     from sqlalchemy import create_engine
#     from config.SystemConfig import SystemConfig
#     from sqlalchemy.pool import NullPool

#     from sqlalchemy.orm import sessionmaker
#     engine = create_engine(SystemConfig.getConfig(
#         'dbconntion'), poolclass=NullPool)
#     # if not database_exists(engine.url):
#     #     create_database(engine.url)
#     # 创建session工厂
#     # DBSession = sessionmaker(bind=engine, expire_on_commit=False)
#     # # 创建session对象
#     # session = DBSession()
#     Base.metadata.create_all(engine)
