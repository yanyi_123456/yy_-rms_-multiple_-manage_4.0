#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:26:19
'''
import sys
sys.path.append(".")
from sqlalchemy import Column, String, Float, Integer
from models.models_base import Base, get_uuid


class EntityHumitureRecord(Base):
    __tablename__ = "rms_humiture_record"
    __table_args__ = (
        {
            "comment": "温湿度记录实体类"
        }
    )
    record_id = Column(String(50), primary_key=True, comment="记录ID", default=get_uuid)  # 记录ID
    device_id = Column(String(50), comment='设备Id')  # 设备ID
    client_id = Column(String(50), comment='终端id')  # 终端ID
    client_name = Column(String(50), comment='终端名称')  # 终端名称
    customer_id = Column(String(50), comment='客户id')  # 客户ID
    temperature = Column(Float, comment='温度值')  # 温度值
    temperature1 = Column(Float, comment='温度值')  # 温度值
    humidity = Column(Float, comment='湿度值')  # 湿度值
    voc_content = Column(Float, comment='voc含量')  # voc含量
    record_date = Column(String(50), comment='记录日期')  # 记录日期
    is_add = Column(Integer, comment='是否添加', default=0)
