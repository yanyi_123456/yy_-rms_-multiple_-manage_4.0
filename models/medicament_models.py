#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:28:11
'''
import sys
sys.path.append('.')
# from common.utils import Utils

from sqlalchemy import Column, String, Integer, Text, Float, inspect
from models.models_base import Base, get_uuid

# from sqlalchemy.ext.declarative import declarative_base

# Base = declarative_base()


class EntityMedicament(Base):
    __tablename__ = "rms_medicament"
    __table_args__ = (
        {
            "comment": "药剂信息实体类"
        }
    )

    medicament_id = Column(String(50), primary_key=True, comment="药剂ID", default=get_uuid)
    variety_id = Column(String(50), comment="药剂类型ID")
    bar_code = Column(String(50), comment="条码")
    client_id = Column(String(50), comment="终端ID")
    client_code = Column(String(50), comment="终端编号")
    customer_id = Column(String(50), comment="客户ID")
    cas_number = Column(String(50), comment="药剂cas码")
    name = Column(String(50), comment="药剂名称")
    english_name = Column(String(50), comment="英文名称")

    manufacturer = Column(String(50), comment="生产厂商")
    distributor = Column(String(50), comment="销售商")
    production_date = Column(String(50), comment="生产日期")
    expiration_date = Column(String(50), comment="过期日期")
    shelf_life = Column(Integer, comment="保质期")
    inventory_warning_value = Column(Integer, comment="库存")
    shelf_life_warning_value = Column(Integer, comment="保质期到期提前预警天数")
    use_days_warning_value = Column(Integer, comment="使用超期预警天数")
    is_weigh = Column(Integer, default=0, comment="是否称重")
    weigh_flag = Column(Integer, default=0, comment="称重标志")

    remain = Column(String(50), comment="库存余量")
    # Speci = Column(Float)  # 规格
    # SpeciUnit = Column(String(50))  # 规格单位
    # Unit = Column(String(50))  # 单位
    total = Column(String(50), comment="总量")
    speci = Column(String(50), comment="规格")
    net_weight_unit = Column(String(50), comment="净含量单位")
    net_weight = Column(String(50), comment="净含量")
    tp = Column(String(50), comment="类型")

    purity = Column(String(50), comment="药剂纯度")
    price = Column(String(50), comment="价格")
    flow_position_code = Column(String(50), comment="试剂所在柜层位置信息")
    cell_position_code = Column(String(50), comment="试剂所在抽屉位置信息")
    place = Column(String(50), comment="位置")
    status = Column(Integer, comment="当前状态（1：在库2：出库3：空瓶）", default=1)
    is_supervise = Column(Integer, comment="是否监管", default=0)
    by_user_date = Column(String(50), comment="最后使用日期")
    by_user_id = Column(String(50), comment="最后使用人ID")
    by_user_name = Column(String(50), comment="最后使用人名称")
    put_in_date = Column(String(50), comment="入库日期")
    put_in_user_id = Column(String(50), comment="入库用户ID")
    put_in_user_name = Column(String(50), comment="入库用户名")
    is_add = Column(Integer, comment="", default=0)
    user_auditing = Column(String(50), comment="用户审计")
    # 单位编号
    unit_code = Column(String(50), comment="单位编号")
    # 农残、兽残、添加剂、易制毒等
    category = Column(String(50), comment="分类")
    # 试剂管理、标准品管理、危化品、试剂耗材
    func_type = Column(String(50), comment="功能分类")
    # 标准品批号
    standard_code = Column(String(50), comment="标准品批号")
    # 存储条件
    storage_condition = Column(String(50), comment="存储条件")
    # 标准品管理分类
    storage_type = Column(String(50), comment="标准品管理分类")

    remark1 = Column(String(50), comment='扩展字段1')
    remark2 = Column(String(50), comment='扩展字段2')
    remark3 = Column(String(50), comment='扩展字段3')
    remark4 = Column(String(50), comment='扩展字段4')
    remark5 = Column(String(50), comment='扩展字段5')
    remark6 = Column(String(50), comment='扩展字段6')
    remark7 = Column(String(50), comment='扩展字段7')
    remark8 = Column(String(50), comment='扩展字段8')
    remark9 = Column(String(50), comment='扩展字段9')
    remark10 = Column(String(50), comment='扩展字段10')
    remark11 = Column(String(50), comment='扩展字段11')
    remark12 = Column(String(50), comment='扩展字段12')
    remark13 = Column(String(50), comment='扩展字段13')
    remark14 = Column(String(50), comment='扩展字段14')
    remark15 = Column(String(50), comment='扩展字段15')
    remark16 = Column(String(50), comment='扩展字段16')
    remark17 = Column(String(50), comment='扩展字段17')
    remark18 = Column(String(50), comment='扩展字段18')
    remark19 = Column(String(50), comment='扩展字段19')
    remark20 = Column(String(50), comment='扩展字段20')
    remark21 = Column(String(50), comment='扩展字段21')
    remark22 = Column(String(50), comment='扩展字段22')
    remark23 = Column(String(50), comment='扩展字段23')
    remark24 = Column(String(50), comment='扩展字段24')
    remark25 = Column(String(50), comment='扩展字段25')
    remark26 = Column(String(50), comment='扩展字段26')
    remark27 = Column(String(50), comment='扩展字段27')
    remark28 = Column(String(50), comment='扩展字段28')
    remark29 = Column(String(50), comment='扩展字段29')
    remark30 = Column(String(50), comment='扩展字段30')



class EntityMedicamentTemplate(Base):
    __tablename__ = "rms_medicament_template"
    __table_args__ = (
        {
            "comment": " 药剂入库模板实体类"
        }
    )
    template_id = Column(String(50), primary_key=True, comment="模板ID", default=get_uuid)
    # 试剂管理、标准品管理、危化品、试剂耗材
    func_type = Column(String(50), comment="功能分类")
    
    customer_id = Column(String(50), comment="客户ID")
    client_id = Column(String(50), comment="终端ID")
    client_name = Column(String(50), comment="终端名称")
    template_name = Column(String(50), comment="模板名称")
    template_content = Column(Text, comment="模板内容")
    is_wait_export = Column(Integer, comment="是否等待导入", default=0)
    template_base64 = Column(Text, comment="模板Base64值")
    start_bar_code = Column(String(50), comment="开始条形码")
    bar_code_count = Column(Integer, comment="条形码数量")
    create_date = Column(String(50), comment="创建日期")
    create_user_id = Column(String(50), comment="创建用户ID")
    create_user_name = Column(String(50), comment="创建用户名")
    is_add = Column(Integer, comment="", default=0)

    @classmethod
    def get_finds(cls):
        return inspect(Base.metadata, cls.__tablename__.c.keys())


class EntityMedicamentRecord(Base):
    __tablename__ = "rms_medicament_record"
    __table_args__ = (
        {
            "comment": "药剂流转记录实体类"
        }
    )
    record_id = Column(
        String(50), primary_key=True, comment="流转记录ID", default=get_uuid)
    client_id = Column(String(50), comment="终端ID")
    client_code = Column(String(50), comment="终端编号")
    customer_id = Column(String(50), comment="客户ID")
    variety_id = Column(String(50), comment="药剂类型ID")
    medicament_id = Column(String(50), comment="药剂ID")
    record_type = Column(Integer, comment="记录类型（1：入库2：领用3：归还）")
    price = Column(Float, comment="药剂价格")
    is_empty = Column(Integer, comment="是否空瓶")
    record_remain = Column(Float, default="0.00", comment="药剂使用余量")
    use_quantity = Column(Float, default="0.00", comment="药剂单次使用量")
    create_date = Column(String(50), comment="创建日期")
    create_user_id = Column(String(50), comment="创建人ID")
    create_user_name = Column(String(50), comment="创建人名称")
    is_add = Column(Integer, comment="", default=0)


class EntityMedicamentVariety(Base):
    __tablename__ = "rms_medicament_variety"
    __table_args__ = (
        {
            "comment": "药剂类型实体类"
        }
    )
    variety_id = Column(
        String(50), primary_key=True, comment="类型ID", default=get_uuid)
    customer_id = Column(String(50), comment="客户ID")
    cas_number = Column(String(50), comment="药剂cas码")
    name = Column(String(50), comment="药剂名称")
    english_name = Column(String(50), comment="英文名称")
    purity = Column(String(50), comment="纯度")
    # Speci = Column(Float)  # 规格
    # SpeciUnit = Column(String(50))  # 规格单位
    # Unit = Column(String(50))  # 单位
    total = Column(String(50), comment="总量")
    net_weight_unit = Column(String(50), comment="净含量单位")
    net_weight = Column(String(50), comment="净含量")
    tp = Column(String(50), comment="类型")

    is_supervise = Column(Integer, comment="是否监管", default=0)
    inventory_warning_value = Column(Integer, comment="库存预警量")
    shelf_life_warning_value = Column(Integer, comment="保质期到期提前预警天数")
    use_days_warning_value = Column(Integer, comment="领用超期预警天数")
    is_weigh = Column(Integer, comment="是否称重")
    empty_count = Column(Integer, comment="空瓶数量")
    use_count = Column(Integer, comment="当前领用数量")
    normal_count = Column(Integer, comment="在库数量")
    total_count = Column(Integer, comment="总数量")
    create_date = Column(String(50), comment="创建日期")
    create_user_id = Column(String(50), comment="创建用户ID")
    create_user_name = Column(String(50), comment="创建用户名称")
    is_add = Column(Integer, comment="", default=0)

    remark1 = Column(String(50), comment='扩展字段1')
    remark2 = Column(String(50), comment='扩展字段2')
    remark3 = Column(String(50), comment='扩展字段3')
    remark4 = Column(String(50), comment='扩展字段4')
    remark5 = Column(String(50), comment='扩展字段5')
    remark6 = Column(String(50), comment='扩展字段6')
    remark7 = Column(String(50), comment='扩展字段7')
    remark8 = Column(String(50), comment='扩展字段8')
    remark9 = Column(String(50), comment='扩展字段9')
    remark10 = Column(String(50), comment='扩展字段10')


class EntityMedicamentExtend(Base):
    __tablename__ = "rms_medicament_extend"
    __table_args__ = (
        {
            "comment": "扩展字段备注表"
        }
    )
    id = Column(String(50), primary_key=True, comment="ID", default=get_uuid)
    name = Column(String(50), comment="名称")
    description = Column(Text, comment="描述")
    key_lenth = Column(String(50), comment="字段长度")
    sort_index = Column(Integer, comment="顺序")
    func_type = Column(String(50), comment="模块类型区分")
    is_use = Column(Integer, default=1, comment="是否启用")
    is_del = Column(Integer, default=0, comment="是否删除")


class EntityMedicamentLabel(Base):
    __tablename__ = "rms_medicament_label"
    __table_args__ = (
        {
            "comment": "扩展字段备注表"
        }
    )
    variety_id = Column(String(50), primary_key=True,
                        comment="ID", default=get_uuid)
    name = Column(String(1000), comment="名称")
    description = Column(Text, comment="描述")
    is_add = Column(Integer, default=0)


class EntityMedicamentRelation(Base):
    __tablename__ = "rms_medicament_relation"
    __table_args__ = (
        {
            "comment": "扩展字段备注表"
        }
    )
    id = Column(String(50), primary_key=True, comment="ID", default=get_uuid)
    name1 = Column(String(255), comment="模块类型区分")
    name2 = Column(String(255), comment="模块类型区分")
    relation = Column(Text, comment="关系")
    description = Column(Text, comment="描述")
    is_add = Column(Integer, default=0)


class EntityStockRecord(Base):
    __tablename__ = "rms_stock_record"
    __table_args__ = (
        {
            "comment": "试剂盘点"
        }
    )
    id = Column(String(50), primary_key=True, comment="ID", default=get_uuid)
    user_id = Column(String(50), comment="用户id")
    create_date = Column(String(50), comment="时间")
    user_name = Column(String(50), comment="用户名称")
    client_code = Column(String(50), comment="柜子码")
    client_id = Column(String(50), comment="柜子id")
    medicament_info = Column(Text, comment="试剂详情")

    is_add = Column(Integer, default=0)
    func_type = Column(Integer, default=0)

class EntityMedicamentMotherLiquor(Base):
    __tablename__ = "rms_medicament_mother_liquor"
    __table_args__ = (
        {
            "comment": "试剂母液配置管理"
        }
    )
    id = Column(String(50), primary_key=True, comment="ID", default=get_uuid)
    medicament_id = Column(String(50), comment="试剂id")
    name = Column(String(255), comment="试剂名称")
    start_time = Column(String(50), comment="配置日期")
    end_time = Column(String(50), comment="有效期")
    purity = Column(String(50), comment="浓度")
    capacity = Column(String(50), comment="配置量")
    solvent = Column(String(50), comment="溶剂")
    basis = Column(Text, comment="配置依据")
    doc_log = Column(Text, comment="配置记录")
    user_id = Column(String(50), comment="配置用户id")
    user_name = Column(String(50), comment="配置用户名称")
    code_number = Column(String(50), comment="条码")
    mother_liquor_code = Column(String(50), comment="母液编码")
    unit_code = Column(Text, comment="单位编号")



if __name__ == '__main__':
    drug_info = {}
    user_info = {}
    from Common.Utils import Utils
    entity = EntityMedicamentVariety(
        customer_id="customer_id",
        name=drug_info.get("name"),
        english_name=drug_info.get("english_name"),
        cas_number=drug_info.get("cas_number"),
        purity=drug_info.get("purity"),
        tp=drug_info.get("tp", 1),
        net_weight_unit=drug_info.get("net_weight_unit"),
        net_weight=drug_info.get("net_weight"),
        create_date=Utils.get_str_datetime(),
        create_user_id=user_info.get("user_id"),
        create_user_name=user_info.get("real_name"),
        shelf_life_warning_value=10,
        inventory_warning_value=10,
        use_days_warning_value=10,
        total_count=1,
        normal_count=1,
        use_count=0,
        empty_count=0,
        is_supervise=0
    )
    print(entity.is_supervise)
    # from db_logic.medicament_record import BllMedicamentRecord
#     # customerId, clientId = None, '8db7e540-070f-11ed-a286-f47b094925e1'
#     # aaa = BllMedicamentRecord().getTodayDrugRecordCount(customerId, clientId)
#     # print(aaa)
#     import random
#     from sqlalchemy import create_engine
#     from sqlalchemy.orm import sessionmaker
#     from config.SystemConfig import SystemConfig
#     from sqlalchemy.pool import NullPool
#     engine = create_engine(SystemConfig.getConfig(
#         'dbconntion'), poolclass=NullPool)
#     DBSession = sessionmaker(bind=engine, expire_on_commit=False)
#     # 创建session对象
#     session = DBSession()
#     client_id= get_uuid()
#     variety_id_list = [get_uuid() for i in range(5)]
#     bar_code_list = [i for i in range(10001, 10110)]
#     customer_id= get_uuid()
#     for i in range(100):
#         db_mo = EntityMedicament(
#             bar_code=random.choice(bar_code_list),
#             variety_id=random.choice(variety_id_list),
#             client_id=client_id,
#             client_code=6,
#             customer_id=customer_id,
#             cas_number='',
#             name='砷',
#             english_name='',
#             shelf_life=5170,
#             inventory_warning_value=10,
#             shelf_life_warning_value=10,
#             use_days_warning_value=10,
#             remain=500,
#             total=500,
#             purity="国标",
#             status=random.randint(1,3)
#             # create_date=Utils.get_str_datetime()
#         )
#         session.add(db_mo)
#         session.commit()

#     # Base.metadata.create_all(engine)

if __name__ == '__main__':
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    from config.SystemConfig import SystemConfig
    from sqlalchemy.pool import NullPool
    engine = create_engine(SystemConfig.getConfig(
        'dbconntion'), poolclass=NullPool)
    DBSession = sessionmaker(bind=engine, expire_on_commit=False)
    # 创建session对象
    session = DBSession()

    Base.metadata.create_all(engine)
