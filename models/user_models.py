#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:29:38
'''
import sys
sys.path.append(".")
from sqlalchemy import Column, String, Text, Integer, BLOB
from Common.Utils import Utils
from models.models_base import Base, get_uuid
from werkzeug.security import check_password_hash, generate_password_hash

# 已同步本地数据库
class EntityUser(Base):
    __tablename__ = "rms_user"
    __table_args__ = (
        {
            "comment": "RMS用户表"
        }
    )

    user_id = Column(String(50), primary_key=True, comment='用户ID', default=get_uuid)  # 用户ID
    customer_id = Column(String(50), comment='客户ID')  # 客户ID
    role_id = Column(String(50), comment='角色ID')  # 角色ID
    role_name = Column(String(50), comment='角色名称')  # 角色名称
    bar_code = Column(String(50), comment='条码')  # 条码
    user_code = Column(String(50), comment='用户编号')  # 用户编号
    account = Column(String(50), comment='账户')  # 账户
    password = Column(String(255), comment='密码')  # 密码
    real_name = Column(String(50), comment='真实名称')  # 真实名称
    avatar_url = Column(String(50), comment='头像地址')  # 头像地址
    avatar_base64 = Column(Text, comment='头像base64值')  # 头像base64值
    sex = Column(Integer, comment='性别')  # 性别 0女，1男
    birthday = Column(String(50), comment='生日')  # 生日
    mobile = Column(String(50), comment='手机号码')  # 手机号码
    email = Column(String(50), comment='Enail')  # Enail
    qq = Column(String(50), comment='QQ')  # QQ
    is_enabled = Column(Integer, comment='是否启用')  # 是否启用
    first_visit_date = Column(String(50), comment='首次登陆日期')  # 首次登陆日期
    last_visit_date = Column(String(50), comment='最后登陆日期')  # 最后登陆日期
    description = Column(String(50), comment='备注')  # 备注
    create_date = Column(String(50), comment='创建日期')  # 创建日期
    create_user_id = Column(String(50), comment='创建用户ID')  # 创建用户ID
    create_user_name = Column(String(50), comment='创建用户名称')  # 创建用户名称
    is_add = Column(Integer, comment='是否添加', default=0)

    def update_last_visit_date(self):
        self.last_visit_date=Utils.get_str_datetime()

    def set_password(self, password):
        '''设置用户密码，保存为 Hash 值'''
        self.password = generate_password_hash(password)

    def check_password(self, password):
        '''验证密码与保存的 Hash 值是否匹配'''
        # return check_password_hash(self.password, password)
        return Utils.MD5(password) == self.password


class EntityRole(Base):
    __tablename__ = "rms_role"
    __table_args__ = (
        {
            "comment": "用户角色实体类"
        }
    )

    role_id = Column(
        String(50), primary_key=True, comment="角色ID", default=get_uuid)
    role_code = Column(String(50), comment="角色编号")
    role_name = Column(String(50), comment="角色名")
    role_level = Column(Integer, comment="角色级别")
    sort_index = Column(Integer, comment="排序序号")
    is_enabled = Column(Integer, comment="是否启用", default=1)
    description = Column(String(50), comment="备注")
    is_add = Column(Integer, comment="", default=0)



# 禁用用户
class EntityUserMedicament(Base):
    __tablename__ = "rms_user_medicament"
    __table_args__ = (
        {
            "comment": "用户试剂权限"
        }
    )       
    id = Column(String(50), primary_key=True, comment="ID", default=get_uuid)
    user_id = Column(String(50),comment="用户ID")
    drug_id = Column(String(50), comment="试剂ID")


class EntityUserFace(Base):
    __tablename__ = "rms_user_face"
    __table_args__ = (
        {
            "comment": "RMS用户人脸数据表"
        }
    )

    face_id = Column(String(50), comment='人脸id',
                     primary_key=True, default=get_uuid)
    customer_id = Column(String(50), comment="客户id")
    user_id = Column(String(50), comment="用户id")
    face_value = Column(BLOB, comment="人脸内容")


# # 用户终端限制
# class EntityClientUser(Base):
#     __tablename__ = "rms_client_user"
#     __table_args__ = (
#         {
#             "comment": "用户终端权限"
#         }
#     )       
#     id = Column(String(50), primary_key=True, comment="ID", default=get_uuid)
#     client_id = Column(String(50),comment="终端id")
#     user_id = Column(String(50), comment="用户ID")


if __name__ == '__main__':
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    from config.SystemConfig import SystemConfig
    from sqlalchemy.pool import NullPool
    engine = create_engine(SystemConfig.getConfig(
        'dbconntion'), poolclass=NullPool)
    DBSession = sessionmaker(bind=engine, expire_on_commit=False)
    # 创建session对象
    session = DBSession()

    Base.metadata.create_all(engine)
