#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/08 14:32:29
'''
import sys
sys.path.append('.')

from sqlalchemy import Text, String, Integer, Column
from models.models_base import Base, get_uuid

class ModelBase(Base):
    __abstract__ = True
    id = Column(String(50), primary_key=True,comment="id", default=get_uuid)
    name = Column(Text, comment="药剂名称")
    english_name = Column(Text, comment="英文名称")
    cas_number = Column(String(50), comment="药剂cas码")
    molecular_formula = Column(String(50), comment="分子式")
    molecular_weight = Column(String(50), comment="分子量")
    physical_data = Column(Text, comment="物理数据")
    toxicity_data = Column(Text, default="", comment="毒性数据")
    use = Column(Text, default="", comment="用途")
    stability = Column(Text, default="", comment="稳定性")
    ecological = Column(Text, default="", comment="生态性")
    precautions = Column(Text, default="", comment="注意事项")

    storage_method = Column(Text, default="", comment="存储方法")
    client_storage_require = Column(Text, default="", comment="柜体存储要求")
    provision = Column(String(50), default="", comment="领用归还规定")


class EntityMsDs(ModelBase):
    __tablename__ = "rms_msds"
    __table_args__ = (
        {
            "comment": "msds数据库"
        }
    )


class EntityDangerous(ModelBase):
    __tablename__ = "rms_dangerous"
    __table_args__ = (
        {
            "comment": "危化品数据库"
        }
    )
    drug_attribute = Column(String(50), comment="试剂属性")
    storage_taboo = Column(Text, comment="存储禁忌")


if __name__ == '__main__':
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    from config.SystemConfig import SystemConfig
    from sqlalchemy.pool import NullPool
    engine = create_engine(SystemConfig.getConfig(
        'dbconntion'), poolclass=NullPool)
    DBSession = sessionmaker(bind=engine, expire_on_commit=False)
    # 创建session对象
    session = DBSession()

    Base.metadata.create_all(engine)
