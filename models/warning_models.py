#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:27:14
'''
import sys
sys.path.append(".")

from sqlalchemy import Column, String, Integer, Text
from models.models_base import Base, get_uuid


class EntityWarning(Base):
    __tablename__ = "rms_warning"
    __table_args__ = (
        {
            "comment": "预警信息实体类"
        }
    )
    warning_id = Column(String(50), primary_key=True, comment="预警ID", default=get_uuid)
    customer_id = Column(String(50), comment="客户ID")
    object_type = Column(String(50), comment="预警对象类型")
    object_id = Column(String(50), comment="预警对象")
    object_name = Column(String(50), comment="对象名称")
    warning_content = Column(String(50), comment="预警内容")
    warning_date = Column(String(50), comment="预警日期")
    warning_user_id = Column(String(50), comment="预警用户ID")
    warning_user_name = Column(String(50), comment="预警用户名", default="系统")
    is_solve = Column(Integer, comment="是否解决", default=0)
    solve_user_id = Column(String(50), comment="解决用户ID")
    solve_user_name = Column(String(50), comment="解决用户名")
    solve_date = Column(String(50), comment="解决日期")
    solve_content = Column(Text, comment="解决内容")
    is_add = Column(Integer, comment="", default=0)
