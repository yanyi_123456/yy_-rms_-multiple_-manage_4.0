#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:21:25
'''
import uuid
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


def get_uuid():
    return str(uuid.uuid1())


def gen_id():

   return uuid.uuid4().hex
