#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:24:43
'''
import sys 
sys.path.append(".")

# 已同步本地数据库
from models.models_base import Base, get_uuid
from sqlalchemy import Column, String, Integer, Float


class EntityClient(Base):
    __tablename__ = "rms_client"
    __table_args__ = ({
        "comment": "终端实体类"
    })

    client_id = Column(String(50), primary_key=True, comment='终端ID', default=get_uuid)  # 终端ID
    client_code = Column(String(50), comment='终端编号')  # 终端编号
    client_name = Column(String(50), comment='终端名称')  # 终端名称
    customer_id = Column(String(50), comment='客户ID')  # 客户ID
    client_type = Column(String(50), comment='类型（库房和药剂柜）')  # 客户类型
    client_title = Column(String(50), comment='设备类型名称')  # 终端标题
    client_use_code = Column(String(50), comment='功能代码')  # 终端编码
    client_speci = Column(String(50), comment='柜子规格（例如6*2 指6层2列）')
    place = Column(String(50), comment='位置')  # 位置
    ip_address = Column(String(50), comment='IP地址')  # IP地址
    contact_people_name = Column(String(50), comment='联系人名称')  # 联系人电话
    contact_phone = Column(String(50), comment='联系人电话')  # 联系人
    total_run_time = Column(Integer, comment='总运行时长')  # 总运行时长
    temperature_control_swich = Column(Integer, comment='温控开关', default=0)
    light_control_swich = Column(Integer, comment='照明灯控制开关', default=0)
    fan_control_swich = Column(Integer, comment='风扇控制开关', default=0)
    temperature_set_value = Column(
        Float, comment='温度设定值', default=0.0)  # 温度设定控温值
    temperature1_set_value = Column(
        Float, comment='温度1设定值', default=0.0)  # 温度设定控温值
    temperature_max_value = Column(
        Float, comment='温度上限值', default=0.0)  # 温度预警上限
    temperature_min_value = Column(
        Float, comment='温度下限值', default=0.0)  # 温度预警下限
    humidity_max_value = Column(Float, comment='湿度上限值', default=0.0)  # 湿度预警上限
    humidity_min_value = Column(Float, comment='湿度下限值', default=0.0)  # 湿度预警下限
    filter_production_date = Column(String(50), comment='药剂柜滤芯生产日期')  # 滤芯生产日期
    filter_shelf_life = Column(Integer, comment='药剂柜滤芯寿命')  # 滤芯保质期
    filter_shelf_life_warning_value = Column(
        Integer, comment='药剂柜滤芯寿命到期前预警天数')  # 滤芯保质期到期提前预警天数
    is_enabled = Column(Integer, comment='有效标记', default=1)  # 是否启用
    sort_index = Column(Integer, comment='排序序号')  # 排序序号
    parent_id = Column(Integer, comment='父级Id')  # 父级ID
    description = Column(String(200), comment='备注')  # 备注
    is_add = Column(Integer, comment='是否添加', default=0)
    func_type = Column(Integer, comment='试剂柜类型', default=0)

class EntityClientCellUser(Base):
    __tablename__ = "rms_client_cell_user"
    __table_args__ = (
        {
            "comment": "柜子抽屉权限实体类"
        }
    )

    id = Column(String(50), primary_key=True, comment="ID", default=get_uuid)
    client_cell_id = Column(String(50), comment="抽屉ID")
    client_cell_code = Column(String(50), comment="抽屉编号")
    client_id = Column(String(50), comment="终端ID")
    client_code = Column(String(50), comment="终端编号")
    user_id = Column(String(50), comment="用户ID")
    is_add = Column(Integer, comment="", default=0)

class EntityClientUser(Base):
    __tablename__ = "rms_client_user"
    __table_args__ = (
        {
            "comment": "用户终端权限实体类"
        }
    )

    client_user_id = Column(String(50), primary_key=True, comment="用户ID", default=get_uuid)
    client_id = Column(String(50), comment="终端ID")
    user_id = Column(String(50), comment="用户ID")


class EntityClientVersion(Base):
    __tablename__ = "rms_client_version"
    __table_args__ = (
        {
            "comment": "终端版本实体类"
        }
    )

    version_id = Column(String(50), primary_key=True, comment="版本ID", default=get_uuid)
    version_name = Column(String(50), comment="版本名称")
    version_code = Column(String(50), comment="版本编号")
    down_link = Column(String(50), comment="下载链接")
    version_info = Column(String(50), comment="版本信息")
    create_date = Column(String(50), comment="创建时间")
    create_user_id = Column(Integer, comment="创建用户ID")
    create_user_name = Column(String(50), comment="创建用户名")
    is_add = Column(Integer, comment="", default=0)


class EntityClientCell(Base):
    __tablename__ = "rms_client_cell"
    __table_args__ = (
        {
            "comment": "终端抽屉类"
        }
    )
    id = Column(String(50), primary_key=True, comment="ID", default=get_uuid)
    cell_code = Column(String(50), comment="抽屉码")
    client_id = Column(String(50), comment="终端id")
    cell_speci = Column(String(50), comment="单元格")


if __name__ == '__main__':
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    from config.SystemConfig import SystemConfig
    from sqlalchemy.pool import NullPool
    engine = create_engine(SystemConfig.getConfig(
        'dbconntion'), poolclass=NullPool)
    DBSession = sessionmaker(bind=engine, expire_on_commit=False)
    # 创建session对象
    session = DBSession()

    Base.metadata.create_all(engine)
