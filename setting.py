#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/26 14:39:54
'''
import sys
sys.path.append('.')


from timing.runtiming import TimingBase


timing_base = TimingBase()
timing_base.start()
# timing_base.timing_status= False
# timing_base.stop()