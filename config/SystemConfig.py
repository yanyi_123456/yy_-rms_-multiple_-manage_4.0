
import os
import configparser

class SystemConfig:
    config = configparser.ConfigParser()
    configPath = os.getcwd() + '/config/system.conf'
    config.read(configPath)

    @classmethod
    def getConfig(cls, key):
        return cls.config.get('system', key)

    @classmethod
    def setConfig(cls, key, value):
        cls.config.set('system', key, value)
        cls.config.write(open(cls.configPath, 'w'))

