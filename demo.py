#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/09/08 13:42:28
'''
import sys
sys.path.append('.')
from db_logic.module import BllModule
from models.power_models import EntityModule
from Common.Utils import Utils
import cv2



def picture_shoot(image_name='img.png', image_path=r'C:/local_project/imgs') -> None:
    '''
    调用摄像头拍照并保存图片到本地
    :param image_name: 图片名字
    :param image_path: 图片保存路径
    :return: None
    '''
    cap = cv2.VideoCapture(0)
    while (cap.isOpened()):
        ret, frame = cap.read()
        # cv2.imshow("Capture_Paizhao", frame) # 显示窗口
        cv2.imwrite(image_path + "\\" + image_name, frame)
        print("保存" + image_name + "成功!")
        break
    cap.release()
    cv2.destroyAllWindows()
 
 
if __name__ == '__main__':
    picture_shoot()