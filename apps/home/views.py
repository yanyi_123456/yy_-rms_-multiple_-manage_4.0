#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 16:19:33
'''
from flask import jsonify, request, g
from db_logic.humiture_record import BllHumitureRecord
from db_logic.medicament import BllMedicament

from models.warning_models import EntityWarning


from db_logic.medicament_record import BllMedicamentRecord
from db_logic.warning import BllWarning
from Common.Utils import PageParam, Utils
from Common.auth import token_auth
from flask import Blueprint


home_router = Blueprint('home', __name__, url_prefix="/home")


# 主概览 当日入库-领用-归还，预警列表， 信息总览
@home_router.route("/home_info", methods=["GET", "POST"])
@token_auth.login_required
def get_home_info():
    func_type = request.values.get("func_type")
    resp_data = {
        "new_day_record_number":{},
        "warning_list": [],
        "warning_data_info": {"count_number":0, "data_list":[]}
    }
    try:
        
        resp_data["new_day_record_number"] = BllMedicamentRecord(
        ).getTodayDrugRecordCount(func_type=func_type)
        warning_obj = BllWarning()
        resp_data["warning_list"] = Utils.msyql_table_model(warning_obj.getWarningList())
        warning_data_list, number_all = warning_obj.get_waring_type_classify()
        resp_data["warning_data_info"] = {
            "count_number": number_all,
            "data_list": warning_data_list
        }
        return jsonify(Utils.true_return(data=resp_data))
    except Exception as error:
        return jsonify(
            Utils.except_return(
                msg=f"error:{error}", 
                data=resp_data
                )
            )


# 试剂余量
@home_router.route("/home_drug_remaining", methods=["GET", "POST"])
@token_auth.login_required
def drug_remaining():
    client_id = request.values.get("client_id")
    func_type = request.values.get("func_type")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)
    data_list = BllMedicament().get_drug_surplus(
        client_id=client_id, 
        func_type=func_type,
        page_param=page_param
        )
    return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": Utils.msyql_table_model(data_list)}))

# 预警信息总览
# @home_router.route("/warning_info_classify", methods=["GET", "POST"])
# def warning_info_classify():
#     data, total_num = BllWarning().get_waring_type_classify()
#     return jsonify(Utils.true_return(data={"total_count": total_num, "data": data}))


# 获取预警信息列表接口
@home_router.route('/home_warning_list', methods=["GET", "POST"])
@token_auth.login_required
def get_warning_list():
    # customer_id = request.values.get('customer_id', '')
    seach_word = request.values.get("seach_word")
    start_time = request.values.get("start_time")
    end_time = request.values.get("end_time")
    object_type = request.values.get("object_type")
    page = int(request.values.get('page', 1))
    page_size = int(request.values.get('page_size', 15))
    page_param = PageParam(page, page_size)
    warning_list = BllWarning().getWarningList(
        pageParam=page_param,
        start_time=start_time,
        end_time=end_time,
        key_word=seach_word,
        object_type=object_type
        )
    data = {
        "total_count": page_param.totalRecords,
        "data_list": Utils.msyql_table_model(warning_list)
    }
    return jsonify(Utils.true_return(data=data))

# 获取预警数量接口
@home_router.route("/home_warning_num", methods=["POST"])
def get_warning_num():
    num = BllWarning().execute("select count(*) num from rms_warning").fetchone().num
    return jsonify(Utils.true_return(data=num))


# 修改预警状态，根据预警id进行
@home_router.route("/update_warning_tp", methods=["GET", "POST"])
@token_auth.login_required
def update_warning_type():

    warning_id = request.values.get('warning_id')
    obj = BllWarning().findEntity(EntityWarning.warning_id == warning_id)
    # obj.object_type = 2
    obj.solve_date= Utils.get_str_datetime()
    obj.solve_user_id = g.current_user.user_id
    obj.solve_user_name = g.current_user.real_name
    obj.is_solve=1
    BllWarning().update(obj)

    return jsonify(Utils.true_return())



# 获取环境控图
@home_router.route("/get_monitoring_info", methods=["GET", "POST"])
@token_auth.login_required
def get_monitoring_info():
    client_id = request.values.get("client_id")
    time_type = request.values.get("time_type")
    obj_type = request.values.get("obj_type")
    data_list = BllHumitureRecord().get_data_info_list(
        client_id=client_id,
        time_type=time_type, 
        obj_type=obj_type
        )
    return jsonify(Utils.true_return(data=Utils.msyql_table_model(data_list)))


