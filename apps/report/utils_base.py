#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/09/14 11:00:47
'''
import sys
sys.path.append('.')

import os
from Common.report_excel import ReportData
from Common.report_excel_new import ReportData as NewReportData
from Common.Utils import Utils
from apps.report.dows_utils import dows_flie

def get_finds_list(tp):
    dic = {
        "1": ["name", "cas_number", "speci", "purity", "count_number", "sum_remain"],
        "2": ['name', 'manufacturer', 'distributor', 'purity', 'cas_number', 'speci', 'remain', 'net_weight', 'net_weight_unit', 'price', 'production_date', 'expiration_date', 'shelf_life', 'unit_code', 'remark2', 'category', 'standard_code', 'storage_condition', 'remark9', 'put_in_user_name', 'put_in_date'],
        "3": ['name', 'unit_code', 'bar_code', 'remark2', 'speci', 'remain', 'purity', 'cas_number', 'production_date', 'expiration_date', 'shelf_life', 'price', 'net_weight', 'net_weight_unit', 'put_in_date', 'manufacturer', 'storage_condition', 'remark9', 'category', 'standard_code', 'put_in_user_name', 'status', 'by_user_name', 'client_name'],
        "4": ["name", "purity", "cas_number", "enter_stock", "in_stock", "up_stock", "no_stock"],
        "5": ["name", "purity", "cas_number", "speci", "on_in", "use_num", "use_level", "ratio"],
        "6": ["create_user_name", "role_name", "use_num", "use_amount", "ratio"],
        "7": ["name", "purity", "cas_number", "speci", "new_on", "sum_tp2", "sum_tp3"],
        "8": ["name", "unit_code", "bar_code", "purity", "cas_number", "use_quantity", "status", "create_date", "create_user_name", "client_name"],
        "9": ["client_name", "place", "day_date", "avg_wd", "avg_sd", "avg_voc"],
        "10": ["day_date", "wd", "sd", "voc",],
        "11": ["name","code_number","unit_code","start_time","end_time","purity","capacity","basis","doc_log","user_name"],
        "12": ["name", "gg_ph", "shelf_life", "manufacturer", "distributor", "num", "acceptace", "put_in_user_name", "put_in_date"]
    }
    return dic.get(str(tp))

def get_finds_name_list(tp):
    dic = {
        "1": ["试剂名称", "CAS码", "规格", "纯度", "在库剩余数量(瓶)", "在库剩余总质量(g)"],
        "2": ['试剂名称', '生产厂商', '供应商', '纯度', 'CAS码', '规格', '余量', '净含量', '单位', '价格', '生产日期', '过期日期', '保质期', '单位编号', '产品编号', '类别', '批号', '存储条件', '密度', '入库操作人', '入库时间'],
        "3": ['试剂名称', '单位编号', '试剂编码', '产品编号', '规格', '余量', '纯度', 'CAS码', '生产日期', '过期日期', '保质期', '价格', '净含量', '单位', '入库日期', '生产厂商', '存储条件', '密度', '类别', '批号', '试剂管理人', '状态', '最后使用人', '所属终端'],
        "4": ["试剂名称", "纯度", "CAS码", "入库数量", "当前在库数量", "当前借出数量", "消耗数量"],
        "5": ["试剂名称", "纯度", "CAS码", "规格", "库存量(瓶)", "使用次数", "总用量(g)", "占用百分比"],
        "6": ["用户名称", "用户身份","使用次数","总用量(g)","占用百分比(%)"],
        "7": ["用户名称", "纯度", "CAS码", "规格", "当前库存总量", "领用次数", "归还次数"],
        "8": ["试剂名称", "单位编号", "试剂编码", "纯度", "CAS码", "用量", "状态", "操作时间", "操作人员", "所属终端"],

        "9": ["柜体名称", "所处位置", "日期", "温度(平均值)", "湿度(平均值)", "VOC(平均值)"],
        "10": ["时间", "温度", "湿度", "VO含量"],
        "11": ["储备液名称","储备液编号","物质单位编号","配置日期","有效期","配置浓度","配置量","配置依据","配置记录", "配置人"],
        "12": ["耗材名称", "规格、批号/型号", "有效期", "生产企业", "经销单位", "数量", "验收记录", "验收人", "日期"]
    }
    return dic.get(str(tp))


def download_file(file_name, data_list, tp):
    try:
        file_path = os.path.join(os.getcwd(), "report_file")
        if not os.path.exists(file_path):
            os.makedirs(file_path)
        if tp == "12":
            obj_report = NewReportData()
        else:
            obj_report = ReportData()
        obj = obj_report.build_file(**{
            "file_name": file_name,
            "data_list": data_list,
            "key_list": get_finds_list(tp),
            "finds_name": get_finds_name_list(tp),
        })
        rsp_file_name = f"{file_name}{Utils.get_file_name_datetime()}"
        obj.save(os.path.join(file_path, rsp_file_name))
        u_path = Utils.getUDiskPath()
        if u_path:
            obj.save(os.path.join(u_path, rsp_file_name))
        rep = dows_flie(file_path, rsp_file_name)
        return rep
        # return file_path, rsp_file_name
        # return True
    except Exception as error:
        print(error)
        return False, False
