import os
from flask import send_file, Response
from urllib.parse import quote
from Common.report_excel import ReportData

def dows_flie(file_path, file_name):
    file_name = file_name+".xlsx"
    # response = send_file(os.path.join(file_path, file_name), as_attachment=True, attachment_filename=quote(file_name))
    
    # response = send_file(ReportData.download_excel(os.path.join(file_path, file_name)), as_attachment=True, attachment_filename=quote(file_name))
    response = Response(ReportData.download_excel(os.path.join(file_path, file_name)), content_type='application/octet-stream')
    response.headers["Content-Disposition"] = \
    "attachment;" \
    "filename*=UTF-8''{utf_filename}".format(
        utf_filename=quote(file_name)
    )
    return response