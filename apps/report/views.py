#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/19 17:12:23
'''
from flask import jsonify, request, g, Blueprint

from Common.Utils import PageParam, Utils
from config.SystemConfig import SystemConfig
from db_logic.medicament_record import BllMedicamentRecord
from db_logic.medicament import BllMedicament
from Common.auth import token_auth
from apps.report.utils_base import download_file

report_router = Blueprint("report", __name__)

"""
# # 获取报表统计页面展示
# # @report_router.route('/report_home', methods=["GET", "POST"])
# # def report_home_info():
# #     # 试剂使用统计
# #     data_list = BllMedicamentRecord().get_drug_record_count()
# #     return jsonify(Utils.true_return(data=data_list))

# # # 消耗统计
# # @report_router.route("/drug_stock_use_classify", methods=["GET", "POST"])
# # def get_drug_stock_use_classify():
# #     data_list, total_number = BllMedicament().get_drug_stock_use_classify()
# #     return jsonify(Utils.true_return(data={"total_count":total_number, "data":data_list}))


# # # 试剂用量消耗
# # @report_router.route("/drug_use_classify", methods=["GET", "POST"])
# # def get_drug_use_classify():
# #     data_list = BllMedicamentRecord().report_home_drug_useing_classify()
# #     return jsonify(Utils.true_return(data=data_list))

# # # 用户试剂消耗
# # @report_router.route("/drug_user_use_info", methods=["GET", "POST"])
# # def get_drug_user_use_number():
# #     data_list = BllMedicamentRecord().report_home_user_use_info()
# #     return jsonify(Utils.true_return(data=data_list))

# # 报表统计主页
# @report_router.route("/report_home", methods=["GET", "POST"])
# @token_auth.login_required
# def report_home_show():
#     resp_data = {
#         "drug_use_type_data": "",
#         "use_avg_info":{},
#         "drug_stock_expend": {"count_number":0, "data":""},
#         "drug_use_expend":"",
#         "user_use_expend": ""

#     }
#     try:
#         obj_data = BllMedicamentRecord()
#         # 试剂使用统计
#         drug_use_type_data, avg_use_dic = obj_data.get_drug_record_count()
#         # 消耗统计部分
#         drug_stock_expend, num_all = BllMedicament().get_drug_stock_use_classify()
#         # 试剂用量消耗
#         drug_use_expend = obj_data.report_home_drug_useing_classify()
#         # 用户试剂消耗
#         user_use_expend = obj_data.report_home_user_use_info()
#         resp_data["drug_use_type_data"] = drug_use_type_data
#         resp_data["use_avg_info"] = avg_use_dic
#         resp_data["drug_stock_expend"] = {
#             "count_number": num_all,
#             "data": drug_stock_expend
#         }
#         resp_data["drug_use_expend"] = drug_use_expend
#         resp_data["user_use_expend"] = user_use_expend
#         return jsonify(Utils.true_return(data=resp_data))
#     except Exception as error:
#         return jsonify(Utils.except_return(msg=f"error:{error}", data=resp_data))





# # 库存信息总览
# @report_router.route("/stock_data_info", methods=["GET", "POST"])
# @token_auth.login_required
# def get_stock_data_info():
#     name = request.values.get("name", None)
#     page = int(request.values.get('page', 1))
#     page_size = int(request.values.get('page_size', 15))
#     page_param = PageParam(page, page_size)
#     data_list = BllMedicament().get_stock_all_info(
#         name=name,
#         page_param=page_param)
#     data_list = Utils.msyql_table_model(data_list)
#     if data_list:
#         return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data_list}))
#     else:
#         return jsonify(Utils.false_return(msg="无效数据"))


# # 入库信息查询and 试剂信息查询
# @report_router.route("/drug_details_info", methods=["GET", "POST"])
# @token_auth.login_required
# def drun_input_info():
#     # 获取参数
#     seach_word = request.values.get('seach_word')
#     manufacturer = request.values.get("manufacturer")
#     start_time = request.values.get("start_time")
#     end_time = request.values.get("end_time")
#     # 客户id
#     customer_id = None
#     client_id = request.values.get("client_id")
#     # 分页处理
#     page = int(request.values.get("page", 1))
#     page_size = int(request.values.get("page_size", 10))
#     page_param = PageParam(page, page_size)
#     # 获取数据
#     data = BllMedicament().getAllDrugList(
#         search_word=seach_word,
#         manufacturer=manufacturer,
#         start_time=start_time,
#         end_time=end_time,
#         page_param=page_param, 
#         customer_id=customer_id, 
#         client_id=client_id
#         )
#     # 数据处理，列表key：value格式

#     data_list = Utils.msyql_table_model(data)
#     return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data_list}))


# # 库存消耗
# # 试剂名称、纯度、cas码 查询rms_medicament 分组后获取
# @report_router.route("/stock_loss_info", methods=["GET", "POST"])
# @token_auth.login_required
# def stock_loss_info():
#     # 获取参数
#     seach_word = request.values.get('seach_word')
#     start_time = request.values.get('start_time')
#     end_time = request.values.get('end_time')
#     page = int(request.values.get("page", 1))
#     page_size = int(request.values.get("page_size", 10))
#     page_param = PageParam(page, page_size)
#     # 获取数据结果
#     data = BllMedicamentRecord().durg_stock_loss(
#         seach_word=seach_word, 
#         start_time=start_time, 
#         end_time=end_time,
#         page_param=page_param)
#     return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data}))



# # 试剂用量消耗
# @report_router.route("/drug_use_expend", methods=["GET", "POST"])
# @token_auth.login_required
# def get_drug_use_expend():
#     # 参数获取
#     seach_word = request.values.get('seach_word')
#     start_time = request.values.get('start_time')
#     end_time = request.values.get('end_time')
#     page = int(request.values.get("page", 1))
#     page_size = int(request.values.get("page_size", 10))
#     page_param = PageParam(page, page_size)
#     # 获取数据
#     data = BllMedicamentRecord().durg_useing_info(
#         seach_word=seach_word,
#         start_time=start_time,
#         end_time=end_time, 
#         page_param=page_param)
#     return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data}))


# # 人员用量消耗
# @report_router.route("/drug_user_use_expend", methods=["GET", "POST"])
# @token_auth.login_required
# def get_drug_user_use_expend():
#     # 参数获取
#     seach_user = request.values.get('seach_user')
#     start_time = request.values.get('start_time')
#     end_time = request.values.get('end_time')
#     page = int(request.values.get("page", 1))
#     page_size = int(request.values.get("page_size", 10))
#     page_param = PageParam(page, page_size)
#     # 获取数据
#     data = BllMedicamentRecord().user_use_info(
#         seach_user=seach_user, 
#         start_time=start_time,
#         end_time=end_time,
#         page_param=page_param)
#     return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data}))


# # 使用频率
# @report_router.route("/use_frequency", methods=["GET", "POST"])
# @token_auth.login_required
# def drug_use_frequency():
#     seach_word = request.values.get('seach_word')
#     client_id = request.values.get("client_id")
#     start_time = request.values.get('start_time')
#     end_time = request.values.get('end_time')
#     page = int(request.values.get("page", 1))
#     page_size = int(request.values.get("page_size", 10))
#     page_param = PageParam(page, page_size)
#     data = BllMedicamentRecord().use_frequency(
#         seach_word=seach_word, 
#         start_time=start_time, 
#         end_time=end_time,
#         client_id=client_id, 
#         page_param=page_param)
#     return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data}))


# # 入库、领用、归还记录
# @report_router.route("/drug_log_type_info", methods=["GET", "POST"])
# @token_auth.login_required
# def drug_log_type_info():
#     page = int(request.values.get("page", 1))
#     page_size = int(request.values.get("page_size", 10))
#     statue_type = request.values.get("status")
#     record_type = request.values.get("record_type")
#     put_in_user_name = request.values.get("user_name")
#     name = request.values.get("name")
#     start_time = request.values.get("start_time")
#     end_time = request.values.get("end_time")
#     page_param = PageParam(page, page_size)

#     data_list = BllMedicament().drug_show_type_info(
#         record_type=record_type, put_in_user_name=put_in_user_name, statue_type=statue_type,
#         name=name, 
#         start_time=start_time, end_time=end_time, 
#         page_param=page_param
#         )
#     data_list = Utils.msyql_table_model(data_list)
#     return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data_list}))
"""

# 报表统计主页
@report_router.route("/report_home", methods=["GET", "POST"])
@token_auth.login_required
def report_home_show():
    func_type = request.values.get("func_type")
    resp_data = {
        "drug_use_type_data": "",
        "use_avg_info":{},
        "drug_stock_expend": {"count_number":0, "data":""},
        "drug_use_expend":"",
        "user_use_expend": ""

    }
    try:
        obj_data = BllMedicamentRecord()
        # 试剂使用统计
        drug_use_type_data, avg_use_dic = obj_data.get_drug_record_count(
            func_type=func_type)
        # 消耗统计部分
        drug_stock_expend, num_all = BllMedicament().get_drug_stock_use_classify(
            func_type=func_type
        )
        # 试剂用量消耗
        drug_use_expend = obj_data.report_home_drug_useing_classify(
            func_type=func_type
        )
        # 用户试剂消耗
        user_use_expend = obj_data.report_home_user_use_info(
            func_type=func_type
        )
        resp_data["drug_use_type_data"] = drug_use_type_data
        resp_data["use_avg_info"] = avg_use_dic
        resp_data["drug_stock_expend"] = {
            "count_number": num_all,
            "data": drug_stock_expend
        }
        resp_data["drug_use_expend"] = drug_use_expend
        resp_data["user_use_expend"] = user_use_expend
        return jsonify(Utils.true_return(data=resp_data))
    except Exception as error:
        return jsonify(Utils.except_return(msg=f"error:{error}", data=resp_data))



# 库存信息总览
@report_router.route("/stock_data_info", methods=["GET", "POST"])
# @token_auth.login_required
def get_stock_data_info():
    name = request.values.get("name", None)
    func_type = request.values.get("func_type")
    page = int(request.values.get('page', 1))
    page_size = int(request.values.get('page_size', 15))
    page_param = PageParam(page, page_size)

    # 导出报表功能
    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None
    data_list = BllMedicament().get_stock_all_info(
        name=name,
        func_type=func_type,
        page_param=page_param)
    data_list = Utils.msyql_table_model(data_list)
    if not download_tp:
        if data_list:
            return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data_list}))
        else:
            return jsonify(Utils.true_return(data={"total_count": 0, "data": data_list}))
    else:
        file_name = request.values.get("file_name", "数据表")
        res = download_file(
            file_name=file_name,
            data_list=data_list,
            tp=download_tp
        )
        if res:
            return res
        else:
            return jsonify(Utils.false_return())


# 入库信息查询and 试剂信息查询
@report_router.route("/drug_details_info", methods=["GET", "POST"])
@token_auth.login_required
def drun_input_info():
    # 获取参数
    seach_word = request.values.get('seach_word')
    manufacturer = request.values.get("manufacturer")
    start_time = request.values.get("start_time")
    end_time = request.values.get("end_time")

    func_type = request.values.get("func_type")

    customer_id = None
    client_id = request.values.get("client_id")
    # TODO 添加房间筛选
    client_place = request.values.get("client_place")
    # 分页处理
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)

    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None
        # 获取数据
    data = BllMedicament().getAllDrugList(
        search_word=seach_word,
        manufacturer=manufacturer,
        func_type=func_type,
        start_time=start_time,
        end_time=end_time,
        page_param=page_param, 
        customer_id=customer_id, 
        client_id=client_id,
        client_speci=client_place
        )
    # 数据处理，列表key：value格式

    data_list = Utils.msyql_table_model(data)
    if not download_tp:
        return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data_list}))
    else:
        file_name = request.values.get("file_name", "数据表")
        rsp = download_file(
            file_name=file_name,
            data_list=data_list,
            tp=download_tp
        )
        if rsp:
            return rsp
        else:
            return jsonify(Utils.false_return())

# 库存消耗
# 试剂名称、纯度、cas码 查询rms_medicament 分组后获取
@report_router.route("/stock_loss_info", methods=["GET", "POST"])
@token_auth.login_required
def stock_loss_info():
    # 获取参数
    seach_word = request.values.get('seach_word')


    func_type = request.values.get("func_type")

    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)

    # 报表数据
    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None

    # 获取数据结果
    data = BllMedicamentRecord().durg_stock_loss(
        seach_word=seach_word, 
        func_type=func_type,
        page_param=page_param)
    if not download_tp:
        return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data}))
    else:
        file_name = request.values.get("file_name", "数据表")
        rsp = download_file(
            file_name=file_name,
            data_list=data,
            tp=download_tp
        )
        if rsp:
            return rsp
        else:
            return jsonify(Utils.false_return())
        



# 试剂用量消耗
@report_router.route("/drug_use_expend", methods=["GET", "POST"])
@token_auth.login_required
def get_drug_use_expend():
    # 参数获取
    seach_word = request.values.get('seach_word')
    func_type = request.values.get("func_type")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)

    # 报表数据
    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None
    # 获取数据
    data = BllMedicamentRecord().durg_useing_info(
        seach_word=seach_word,
        func_type=func_type,
        page_param=page_param)
    if not download_tp:
        return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data}))
    else:
        file_name = request.values.get("file_name", "数据表")
        rsp = download_file(
            # file_path=file_path,
            file_name=file_name,
            data_list=data,
            tp=download_tp
        )
        if rsp:
            return rsp
        else:
            return jsonify(Utils.false_return())

# 人员用量消耗
@report_router.route("/drug_user_use_expend", methods=["GET", "POST"])
@token_auth.login_required
def get_drug_user_use_expend():
    # 参数获取
    seach_user = request.values.get('seach_user')
    start_time = request.values.get('start_time')
    end_time = request.values.get('end_time')
    func_type = request.values.get("func_type")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)

    # 报表数据
    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None
    # 获取数据
    data = BllMedicamentRecord().user_use_info(
        seach_user=seach_user, 
        func_type=func_type,
        start_time=start_time,
        end_time=end_time,
        page_param=page_param)
    if not download_tp:
        return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data}))
    else:
        file_name = request.values.get("file_name", "数据表")
        rsp = download_file(
            file_name=file_name,
            data_list=data,
            tp=download_tp
        )
        if rsp:
            return rsp
        else:
            return jsonify(Utils.false_return())

# 使用频率
@report_router.route("/use_frequency", methods=["GET", "POST"])
@token_auth.login_required
def drug_use_frequency():
    seach_word = request.values.get('seach_word')
    client_id = request.values.get("client_id")
    start_time = request.values.get('start_time')
    end_time = request.values.get('end_time')
    func_type = request.values.get("func_type")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)

    # 报表数据
    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None

    data = BllMedicamentRecord().use_frequency(
        seach_word=seach_word, 
        start_time=start_time, 
        end_time=end_time,
        func_type=func_type,
        client_id=client_id, 
        page_param=page_param)
    if not download_tp:
        return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data}))
    else:
        file_name = request.values.get("file_name", "数据表")
        rsp = download_file(
            # file_path=file_path,
            file_name=file_name,
            data_list=data,
            tp=download_tp
        )
        if rsp:
            return rsp
        else:
            return jsonify(Utils.false_return())

# 入库、领用、归还记录
@report_router.route("/drug_log_type_info", methods=["GET", "POST"])
# @token_auth.login_required
def drug_log_type_info():
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    statue_type = request.values.get("status")
    record_type = request.values.get("record_type")
    func_type = request.values.get("func_type")

    put_in_user_name = request.values.get("user_name")
    name = request.values.get("name")
    start_time = request.values.get("start_time")
    end_time = request.values.get("end_time")
    page_param = PageParam(page, page_size)

    # 报表数据
    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None
    data_list = BllMedicament().drug_show_type_info(
        record_type=record_type, 
        put_in_user_name=put_in_user_name, 
        func_type=func_type,
        statue_type=statue_type,
        name=name, 
        start_time=start_time, end_time=end_time, 
        page_param=page_param
        )
    data_list = Utils.msyql_table_model(data_list)
    if not download_tp:
        return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": data_list}))
    else:
        file_name = request.values.get("file_name", "数据表")
        rsp = download_file(
            # file_path=file_path,
            file_name=file_name,
            data_list=data_list,
            tp=download_tp
        )
        if rsp:
            return rsp
        else:
            return jsonify(Utils.false_return())


# 入库验收表导出
@report_router.route("/putin_acceptance_record", methods=["GET", "POST"])
# @token_auth.login_required
def get_putin_acceptance_record():
    data = BllMedicament().execute(
        """
        SELECT 
            `name`, speci, remark5, date_format( expiration_date,'%Y-%m-%d') shelf_life, manufacturer, distributor, count(*) num, put_in_user_name, date_format( put_in_date,'%Y-%m-%d') put_in_date
        FROM `rms_medicament` 
        GROUP BY `name`, purity, speci, remark5, DATE_FORMAT(put_in_date,"%Y-%m-%d")
        """
    ).fetchall()
    data_list = Utils.msyql_table_model(data)
    for i in data_list:
        gg_ph = ''
        if str(i["speci"]) !='' and str(i["speci"]) !='None':
            gg_ph += str(i["speci"])
        if str(i["remark5"]) !='' and str(i["remark5"]) !='None':
            if gg_ph:
                gg_ph += ","
            gg_ph += str(i["remark5"])
        i["gg_ph"] = gg_ph
        i["acceptace"] = """□标识清晰\r\n□外观完整、无破损\r\n□形状无明显改变\r\n□符合申购要求"""
        i["shelf_life"] = str(i["shelf_life"])
    file_name = "实验耗材入库验收记录表"
    data = download_file(
        # file_path=file_path,
        file_name=file_name,
        data_list=data_list,
        tp="12"
    )
    if data:
        return data
    else:
        return jsonify(Utils.false_return())