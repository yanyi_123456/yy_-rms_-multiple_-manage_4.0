#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date: 2022/11/28 11:17:55
'''
import sys
sys.path.append('.')

import json
from flask import jsonify, request, g, Blueprint

from Common.Utils import PageParam, Utils
from Common.auth import token_auth
from db_logic.stock_record import BllStockRecord
from db_logic.medicament import BllMedicament
from models.medicament_models import EntityStockRecord
stock_router = Blueprint("stock_record", __name__)




# 列表展示
@stock_router.route("/getlist", methods=["POST"])
# @token_auth.login_required
def get_stock_record_list():
    client_id = request.values.get("client_id")
    func_type = request.values.get("func_type")
    page = request.values.get("page", 1)
    page_size = request.values.get("page_size", 10)
    page_param = PageParam(int(page), int(page_size))
    data_list = BllStockRecord().get_list_info(client_id=client_id, page_param=page_param, func_type=func_type)
    data_list = Utils.msyql_table_model(data_list)
    return jsonify(Utils.true_return(data={"data_list": data_list, "total_count": page_param.totalRecords}))

# 获取盘点用户输入界面
@stock_router.route("/get_drug_list", methods=["POST"])
def get_drug_group_list():
    client_id = request.values.get("client_id")
    data = BllMedicament().get_drug_stock_info(client_id=client_id)
    data_list = [{"name":i.name, "input_num":0} for i in data]
    return jsonify(Utils.true_return(data=data_list))


# 盘点库单
@stock_router.route("/set_stock", methods=["POST"])
@token_auth.login_required
def set_stock_record():
    client_id = request.values.get("client_id")
    client_code = request.values.get("client_code")
    stock_info = request.values.get("stock_info")
    func_type = request.values.get("func_type")
    stock_info = json.loads(stock_info)
    drug_info = BllMedicament().get_drug_stock_info(client_id=client_id)
    drug_info_list = Utils.msyql_table_model(drug_info)
    medicament_info = []
    for i in drug_info_list:
        new_dict = {**i}
        new_dict["input_num"] = int(stock_info.get(i["name"], 0))
        new_dict['difference_num'] = int(stock_info.get(i["name"], 0)) - int(i["stock_num"])
        medicament_info.append(new_dict)
    data = BllStockRecord().set_stock_info(
        client_id=client_id,
        client_code=client_code,
        user_info=g.current_user,
        func_type=func_type,
        medicament_info=medicament_info,
        )
    if data:
        return jsonify(Utils.true_return(msg="录入成功！"))
    else:
        return jsonify(Utils.false_return(msg=data))

# 展示判断数据
@stock_router.route("/get_stock_info", methods=["POST"])
def stock_info():
    stock_id = request.values.get("id")
    obj = BllStockRecord().findEntity(EntityStockRecord.id==stock_id)
    if obj:
        data = json.loads(obj.medicament_info)
        return jsonify(Utils.true_return(data=data))
    else:
        return jsonify(Utils.false_return(msg="信息有误！"))


# 展示判断数据
@stock_router.route("/del_stock", methods=["POST"])
def del_stock_info():
    stock_id = request.values.get("id")
    try:
        BllStockRecord().delete(EntityStockRecord.id==stock_id)
        return jsonify(Utils.true_return())
    except Exception as error:
        return jsonify(Utils.false_return(msg=f"信息有误:{error}"))