#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/12 15:08:24
'''
import sys
sys.path.append('.')


from flask import jsonify, request, g
from flask import Blueprint
from Common.auth import token_auth
from Common.Utils import Utils, PageParam
from models.medicament_models import EntityMedicamentExtend
from db_logic.medicament_extend import BllMedicamentExtend

drug_form_router = Blueprint("drug_form", __name__)


@drug_form_router.route("/getlist", methods=["GET", "POST"])
@token_auth.login_required
def get_drug_form():
    page = request.values.get("page", 1)
    page_size = request.values.get("page_size", 10)
    func_type = request.values.get("func_type")
    page_param = PageParam(int(page), int(page_size))
    data_list = BllMedicamentExtend().get_list(func_type=func_type,page_param=page_param)
    return jsonify(Utils.true_return(data={"data_list": Utils.msyql_table_model(data_list), "total_count": page_param.totalRecords}))


@drug_form_router.route("/add_update", methods=["GET", "POST"])
@token_auth.login_required
def add_update_drug_form():
    drug_form_id = request.values.get("drug_form_id")
    func_type = request.values.get("func_type")
    bll_obj = BllMedicamentExtend()
    finds = [
        "name", "description", "key_lenth", "is_use"
    ]
    if drug_form_id:
        obj = bll_obj.findEntity(EntityMedicamentExtend.id == drug_form_id)
        if obj:
            for i in finds:
                setattr(obj, i, request.values.get(i))
            bll_obj.update(obj)
            return jsonify(Utils.true_return())
        else:
            return jsonify(Utils.false_return(msg="表单id有误"))
    else:
        del_obj = bll_obj.execute(
            "select id, min(sort_index) from rms_medicament_extend where is_del=1").first()
        if del_obj[0]:
            obj = bll_obj.findEntity(EntityMedicamentExtend.id == del_obj[0])
            for i in finds:
                setattr(obj, i, request.values.get(i))
            setattr(obj, "func_type", func_type)
            setattr(obj, 'is_del', 0)
            bll_obj.update(obj)
        else:
            count_num = bll_obj.execute(
                "select count(id) count_num from rms_medicament_extend").fetchone()
            if count_num.count_num < 30:
                obj = EntityMedicamentExtend()
                for i in finds:
                    setattr(obj, i, request.values.get(i))
                sort_index = bll_obj.execute(
                    "select max(sort_index) sort_index from rms_medicament_extend").fetchone()
                setattr(obj, "sort_index", int(sort_index.sort_index or '0') + 1)
                setattr(obj, "func_type", func_type)
                bll_obj.insert(obj)
                return jsonify(Utils.true_return())
            else:
                return jsonify(Utils.false_return())

@drug_form_router.route("/del", methods=["GET", "POST"])
@token_auth.login_required
def del_drug_form():
    drug_form_id = request.values.get("drug_form_id")
    obj = BllMedicamentExtend().findEntity(EntityMedicamentExtend.id==drug_form_id)
    if obj:
        obj.is_del=1
        BllMedicamentExtend().update(obj)
        return jsonify(Utils.true_return(msg="删除成功"))
    else:
        return jsonify(Utils.false_return(msg="该表单不存在"))


@drug_form_router.route("/get_use", methods=["GET"])
@token_auth.login_required
def get_use_drug_form():
    func_type = request.values.get("func_type")
    data_list = BllMedicamentExtend().get_use_list(func_type=func_type)
    return jsonify(Utils.true_return(data=Utils.msyql_table_model(data_list)))
