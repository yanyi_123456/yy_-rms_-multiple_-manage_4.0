#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/01 17:04:40
'''
import sys
sys.path.append('.')

from sqlalchemy import and_, or_
from db_logic.module import BllModule
from db_logic.role import BllRole
from Common.auth import token_auth
from flask import Blueprint, jsonify, request, g
from db_logic.module_relation import BllModuleRelation

from db_logic.user import BllUser

from models.power_models import EntityModuleRelation

from models.user_models import EntityRole, EntityUser
from Common.Utils import PageParam, Utils


power_router = Blueprint('power', __name__)

# 获取用户权限
@power_router.route("/get_user_power_list", methods=["GET", "POST"])
@token_auth.login_required
def get_user_power_list():
    user_id = request.values.get("user_id")
    if user_id:
        user_info = BllUser().findEntity(EntityUser.user_id == user_id)
        data_list = BllModuleRelation().get_user_module_list(user_info)
        return jsonify(Utils.true_return(data=data_list))
    return jsonify(Utils.false_return(msg="用户id不能为空"))

# 获取用户是否用用户管理权限
@power_router.route("/get_user_manage_auth", methods=["POST"])
@token_auth.login_required
def user_usermanage_auth():
    user_info = g.current_user
    data = BllModuleRelation().get_user_manage_auth(user_info)
    return jsonify(Utils.true_return(data=data))


# 分配用户权限
@power_router.route("/add_user_power", methods=["GET", "POST"])
@token_auth.login_required
def add_user_power_list():
    user_id = request.values.get("user_id")
    client_module = request.values.get("client_module_id")
    drug_module = request.values.get("drug_module_id")
    standard_module = request.values.get("standard_module_id")
    consumables_module = request.values.get("consumables_module_id")
    instrument_module = request.values.get("instrument_module_id")
    inster_dic = {
        "1": eval(client_module) if client_module else [],
        "2": eval(drug_module) if drug_module else [],
        "3": eval(standard_module) if standard_module else [],
        "4": eval(consumables_module) if consumables_module else [],
        "5": eval(instrument_module) if instrument_module else [],
    }
    # 删除除去柜子权限外的所有权限
    BllModuleRelation().delete(
        EntityModuleRelation.object_id == user_id
    )
    inster_module_list = []
    for k,v in inster_dic.items():
        for i in v:
            inster_module_list.append(
                EntityModuleRelation(
                    object_type=2,
                    object_id=user_id,
                    module_id=i,
                    module_type=k,
                    create_date=Utils.get_str_datetime(),
                    create_user_id=g.current_user.user_id,
                    create_user_name=g.current_user.real_name
                )
            )
    BllModuleRelation().insert_many(inster_module_list)
    return jsonify(Utils.true_return())


# 获取登录用户权限
@power_router.route("/user_power", methods=["GET", "POST"])
@token_auth.login_required
def get_login_user_power_list():
    user_info = g.current_user
    # user_id = '4cea74ee-0d8b-11ed-943e-f47b094925e1'
    # user_info = BllUser().findEntity(user_id)
    module_list = BllModuleRelation().get_login_user_module_list(
        user_id=user_info.user_id, role_id=user_info.role_id)
    # module_list = Utils.msyql_table_model(module_list)
    return jsonify(Utils.true_return(data=module_list))

# 获取角色标签列表
@power_router.route("/get_role_name_list", methods=["GET", "POST"])
@token_auth.login_required
def get_role_name_list():
    data_list = BllModuleRelation().get_role_name_list()
    return jsonify(Utils.true_return(data=Utils.msyql_table_model(data_list)))

# 获取角色列表
@power_router.route("/get_list", methods=["GET", "POST"])
@token_auth.login_required
def get_power_list():
    # 根据角色表进行关联查询，
    # 思路：角色表角色id->模块权限表（object_id）-> 终端权限表（）
    #                   |                       |
    #            role_id=object_id     module_id=module_id   
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 15)) 
    page_param = PageParam(page, page_size)       
    data_list = BllModuleRelation().get_role_module(page_param)

    return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data_list": Utils.msyql_table_model(data_list)}))

# 通过角色id查询角色列表
@power_router.route("/get_role_module", methods=["GET", "POST"])
@token_auth.login_required
def get_role_module_list():
    role_id = request.values.get("role_id")
    data_list = BllModuleRelation().get_role_module_list(role_id)
    return jsonify(Utils.true_return(data=Utils.msyql_table_model(data_list)))


# 获取模块列表
@power_router.route("/get_module_list", methods=["GET", "POST"])
# @token_auth.login_required
def get_module_list():
    data_list = BllModule().get_module_list()
    return jsonify(Utils.true_return(data=data_list))

# 添加角色
@power_router.route("/add_update_role", methods=["GET", "POST"])
@token_auth.login_required
def add_role():
    role_id = request.values.get("role_id")
    role_code = request.values.get("role_code")
    role_name = request.values.get("role_name")
    client_module = request.values.get("client_module_id")
    drug_module = request.values.get("drug_module_id")
    standard_module = request.values.get("standard_module_id")
    consumables_module = request.values.get("consumables_module_id")
    instrument_module = request.values.get("instrument_module_id")
    # instrument_modulet = request.values.get("instrument_module_id")
    inster_dic = {
        "1": eval(client_module) if client_module else [],
        "2": eval(drug_module) if drug_module else [],
        "3": eval(standard_module) if standard_module else [],
        "4": eval(consumables_module) if consumables_module else [],
        "5": eval(instrument_module) if instrument_module else [],
    }
    description = request.values.get("description")
    # 获取角色代码或者角色名的角色信息
    obj = BllRole().findEntity(
        or_(EntityRole.role_code == role_code, EntityRole.role_name == role_name))
    # 判断是创建角色
    if not role_id:
        # 判断信息重复
        if obj:
            return jsonify(Utils.false_return(msg="角色代码或角色名重复，创建失败"))
        else:
            # 创建角色
            role_max_obj = BllRole().execute("select max(sort_index) as sort_index from rms_role").fetchone()
            role_id=Utils.UUID()
            role_obj = EntityRole(
                role_id=role_id,
                role_code=role_code,
                role_name=role_name,
                role_level=1,
                description=description,
                sort_index=int(role_max_obj.sort_index or '0') + 1
            )
            BllRole().insert(role_obj)
    else:
        # 修改角色信息
        role_obj = BllRole().findEntity(role_id)
        # 判断修改的信息是否重复
        if not obj or role_obj.role_id == role_id:
            role_obj.role_code=role_code,
            role_obj.role_name=role_name,
            role_obj.description=description,
            BllRole().update(role_obj)
        else:
            return jsonify(Utils.false_return(msg="角色代码或角色名重复，修改失败"))
    # 删除原有的权限，每次重新创建
    BllModuleRelation().delete(EntityModuleRelation.object_id==role_id)
    inster_module_list = []
    for k,v in inster_dic.items():
        for i in v:
            inster_module_list.append(
                EntityModuleRelation(
                    object_type=1,
                    object_id=role_id,
                    module_id=i,
                    module_type=k,
                    create_date=Utils.get_str_datetime(),
                    create_user_id=g.current_user.user_id,
                    create_user_name=g.current_user.real_name
                )
            )
    BllModuleRelation().insert_many(inster_module_list)
    return jsonify(Utils.true_return())

# 删除角色
@power_router.route("/del_role", methods=["GET", "POST"])
@token_auth.login_required
def remove_role():
    role_id = request.values.get("role_id")
    if role_id:
        BllRole().delete(EntityRole.role_id==role_id)
        BllModuleRelation().delete(EntityModuleRelation.object_id==role_id)
        return jsonify(Utils.true_return(msg="删除成功"))
    return jsonify(Utils.false_return(msg="请选择角色"))