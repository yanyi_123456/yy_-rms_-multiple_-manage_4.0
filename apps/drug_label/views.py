#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date: 2022/11/28 10:51:52
'''
import sys
sys.path.append('.')
import json
from flask import Blueprint, request, jsonify

from db_logic.medicament_label import BllMedicamentLabel
from db_logic.medicament_relation import BllMedicamentRelation

from models.medicament_models import EntityMedicamentLabel, EntityMedicamentRelation

from Common.Utils import PageParam, Utils

drug_label_app = Blueprint("drug_label", __name__)


@drug_label_app.route("/get", methods=["POST"])
# @token_auth.login_required
def get_seach_drug_label():
    seach_word = request.values.get("seach_word")
    page = request.values.get("page", 1)
    page_size = request.values.get("page_size", 10)
    page_param = PageParam(int(page), int(page_size))

    data_list = BllMedicamentLabel().get_seach_list(
        seach_word=seach_word,
        page_param=page_param
    )
    return jsonify(Utils.true_return(data={
        "total_count": page_param.totalRecords,
        "data_list": Utils.msyql_table_model(data_list)
    }))


@drug_label_app.route("/attribute_label_list", methods=["POST"])
def get_attribute_label_list():
    data_list = []
    data = BllMedicamentLabel().get_attribute_label_list_info()
    for i in data:
        for j in i[0].split(","):
            if j not in data_list:
                data_list.append(j)
    return jsonify(
        Utils.true_return(
            data=data_list
        )
    )

@drug_label_app.route("/update_add", methods=["POST"])
# @token_auth.login_required
def set_drug_label():
    variety_id = request.values.get("id")
    name = request.values.get("name")
    description = request.values.get("description")
    obj = BllMedicamentLabel()
    if variety_id:
        label_obj = obj.findEntity(
            EntityMedicamentLabel.variety_id == variety_id)
        if label_obj:
            setattr(label_obj, "name", name)
            setattr(label_obj, "description", description)
            obj.update(label_obj)
            return jsonify(Utils.true_return(msg="修改成功"))
        else:
            return jsonify(Utils.false_return())
    else:
        model_obj = EntityMedicamentLabel(
            name=name,
            description=description
        )
        obj.insert(model_obj)
        return jsonify(Utils.true_return(msg="添加成功"))




@drug_label_app.route("/del", methods=["POST"])
# @token_auth.login_required
def del_drug_label():
    label_id = request.values.get("id_list")
    obj = BllMedicamentLabel()
    data_list = obj.findList(
        EntityMedicamentLabel.variety_id.in_(tuple(json.loads(label_id)))).all()
    for i in data_list:
        obj.session.delete(i)
    obj.session.commit()
    return jsonify(Utils.true_return(msg="删除成功"))


@drug_label_app.route("/relation/get", methods=["POST"])
# @token_auth.login_required
def get_seach_drug_relation():
    seach_word = request.values.get("seach_word")
    page = request.values.get("page", 1)
    page_size = request.values.get("page_size", 10)
    page_param = PageParam(int(page), int(page_size))

    data_list = BllMedicamentRelation().get_seach_list(
        seach_word=seach_word,
        page_param=page_param
    )
    return jsonify(Utils.true_return(data={
        "total_count": page_param.totalRecords,
        "data_list": Utils.msyql_table_model(data_list)
    }))


@drug_label_app.route("/relation/update_add", methods=["POST"])
# @token_auth.login_required
def set_drug_relation():
    relation_id = request.values.get("id")
    name = request.values.get("name1")
    name2 = request.values.get("name2")
    relation = request.values.get("relation")
    description = request.values.get("description")
    obj = BllMedicamentRelation()
    if relation_id:
        relation_obj = obj.findEntity(
            EntityMedicamentRelation.id == relation_id)
        if relation_obj:
            setattr(relation_obj, "name1", name)
            setattr(relation_obj, "name2", name2)
            setattr(relation_obj, "relation", relation)
            setattr(relation_obj, "description", description)
            obj.update(relation_obj)
            return jsonify(Utils.true_return(msg="修改成功"))
        else:
            return jsonify(Utils.false_return())
    else:
        model_obj = EntityMedicamentRelation()
        setattr(model_obj, "name1", name)
        setattr(model_obj, "name2", name2)
        setattr(model_obj, "relation", relation)
        setattr(model_obj, "description", description)
        obj.insert(model_obj)
        return jsonify(Utils.true_return(msg="添加成功"))


@drug_label_app.route("/relation/del", methods=["POST"])
# @token_auth.login_required
def del_relation():
    list_id = request.values.get("id_list")
    obj = BllMedicamentRelation()
    data_list = obj.findList(
        EntityMedicamentRelation.id.in_(tuple(json.loads(list_id)))).all()
    for i in data_list:
        obj.session.delete(i)
    obj.session.commit()
    return jsonify(Utils.true_return(msg="删除成功"))

