#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/26 11:19:30
'''
import datetime
from flask import jsonify, request, g
from flask import Blueprint
from db_logic.client import BllClient
from db_logic.dangerous import BllDangerous


from db_logic.medicament import BllMedicament
from db_logic.medicament_record import BllMedicamentRecord
from db_logic.msds import BllMsDs
from db_logic.user_medicament import BllUserMedicament
from models.medicament_models import EntityMedicament
from models.client_models import EntityClient
from Common.Utils import DrugStatus, PageParam, Utils
from Common.auth import token_auth

drug_router = Blueprint("drug", __name__)




# 试剂扫码领用
@drug_router.route("/use", methods=["GET", "POST"])
@token_auth.login_required
def drug_collect():
    # 获取参数
    bar_code = request.values.get("bar_code")
    func_type = request.values.get("func_type")
    # user_id=request.values.get("user_id")
    # 默认用户id，后续从token 中获取
    # user_id = '4cea74ee-0d8b-11ed-943e-f47b094925e1'
    # 强制使用，默认0不强制，1强制
    force_use = request.values.get("force_use")
    force_use = force_use if force_use != '' else '0'

    # 获取条码试剂
    drug_entity = BllMedicament().findEntity(EntityMedicament.bar_code==bar_code)
    # 条码实际逻辑判段
    if not drug_entity:
        data = Utils.false_return(msg="药剂条码无效")
    elif drug_entity.status not in [DrugStatus.Normal, DrugStatus.Empty]:
        data = Utils.false_return(msg="药剂未在库")
    elif drug_entity.status == DrugStatus.Empty:
        data = Utils.false_return(msg="药剂已为空瓶")
    elif str(drug_entity.func_type) != str(func_type):
        data = Utils.false_return(msg="试剂不属于这里当前管理")
    elif drug_entity.remain == '' or drug_entity.remain == None or not drug_entity:
        data = Utils.false_return(msg="试剂未称重,请先称重！")
    else:
        customer_id = drug_entity.customer_id
        client_id = drug_entity.client_id

        # 获取临期的试剂
        drug_obj = BllMedicament().getDrugNearExpired(drug_entity.variety_id, customer_id)
        # date_str = lambda x: datetime.datetime.strptime('', "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d")
        # drug_obj_date_str = date_str(drug_obj.expiration_date)
        drug_obj_date_str = None
        if drug_obj.expiration_date:
            drug_obj_date_str = datetime.datetime.strptime(drug_obj.expiration_date, "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d")
        # drug_entity_date_str = date_str(drug_entity.expiration_date)
        drug_entity_date_str = None
        if drug_entity.expiration_date:
            drug_entity_date_str = datetime.datetime.strptime(drug_entity.expiration_date, "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d")
        if drug_obj.medicament_id != drug_entity.medicament_id and force_use == "False" and drug_obj_date_str != drug_entity_date_str:
            data = Utils.false_return(status=2, msg=f'"{drug_entity.name}({drug_entity.bar_code})"此类药有更接近保质期的实例"{drug_obj.bar_code}", 存在于"{drug_obj.client_code}"号终端')
        else:
            # 领取试剂
            # user_info = BllUser().findEntity(EntityUser.user_id==user_id)
            user_info = g.current_user
            drug_entity.by_user_date = Utils.get_str_datetime()
            drug_entity.by_user_id=user_info.user_id
            drug_entity.by_user_name=user_info.real_name
            drug_entity.status=DrugStatus.Out
            # print(Utils.to_dict(drug_entity))
            # for k,v in drug_entity:
            #     print(k,v)
            # 试剂领用--事务逻辑处理
            BllMedicament().drugUse(
                drug_entity, 
                BllClient().findEntity(EntityClient.client_id==client_id),
                user_info
                )
            data = Utils.true_return(msg='领用成功', data=Utils.to_dict(drug_entity))
    return jsonify(data)


# 试剂归还
@drug_router.route("/drug_return", methods=["GET", "POST"])
@token_auth.login_required
def drug_return():
    # 获取参数
    bar_code = request.values.get("bar_code")
    func_type = request.values.get("func_type")
    place = request.values.get("place")
    # user_id = request.values.get("user_id")
    # 默认用户id，后续从token 中获取
    # user_id = '4cea74ee-0d8b-11ed-943e-f47b094925e1'
    
    
    # 根据条码查询试剂信息
    drug_info = BllMedicament().findEntity(EntityMedicament.bar_code==bar_code)
    if not drug_info:
        data = Utils.false_return(msg="药剂条码无效")
    elif drug_info.status != DrugStatus.Out:
        data = Utils.false_return(msg="此药剂未被领用")
    elif drug_info.func_type != func_type:
        data = Utils.false_return(msg="试剂不属于这里当前管理")
    else:
        # 药剂归还，进入归还事务处理
        drug_info.status = DrugStatus.Normal
        if place:
            drug_info.client_id = place
        # user_info = BllUser().findEntity(EntityUser.user_id == user_id)
        user_info = g.current_user
        BllMedicament().drugReturn(drug_info, BllClient().findEntity(drug_info.client_id), user_info)
        data = Utils.true_return(msg="药剂归还成功", data=Utils.to_dict(drug_info))
    return jsonify(data)


# 试剂领用/归还，列表展示
@drug_router.route("/use_or_return_list", methods=["GET", "POST"])
@token_auth.login_required
def drug_use_retur_list():
    status = request.values.get("status")
    func_type = request.values.get("func_type")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)
    data = BllMedicament().drug_use_return(status=status, func_type=func_type, page_param=page_param)
    return jsonify(Utils.true_return(data={"total_count": page_param.totalRecords, "data": Utils.msyql_table_model(data)}))
    

# 试剂称重展示试剂详细信息
@drug_router.route("/get_drug_info", methods=["GET", "POST"])
@token_auth.login_required
def get_drug_info():
    bar_code = request.values.get("bar_code")
    func_type = request.values.get("func_type")
    bar_code, new_code = Utils.get_bar_code_reverse(bar_code)
    data = BllMedicament().execute(
        f"select * from rms_medicament where func_type={func_type} and (bar_code like '%{bar_code}%' or bar_code like '%{new_code}%') "
        ).fetchall()
    data = Utils.msyql_table_model(data)
    if data:
        return jsonify(Utils.true_return(data=data[0]))
    else:
        return jsonify(Utils.false_return(msg="条码有误"))
    




# 试剂管理
@drug_router.route("/drug_list", methods=["GET", "POST"])
@token_auth.login_required
def get_drug_manage():
    seach_word = request.values.get("seach_word")
    b_code = request.values.get("b_code")
    client_id = request.values.get("client_id")
    func_type = request.values.get("func_type")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)
    data_list = BllMedicament().get_drug_list(
        seach_word=seach_word, 
        b_code=b_code,
        client_id=client_id, func_type=func_type, page_param=page_param
        )
    return jsonify(Utils.true_return(data={"data": Utils.msyql_table_model(data_list), "total_count":page_param.totalRecords}))




# 编辑数据
@drug_router.route("/update", methods=["GET", "POST"])
@token_auth.login_required
def drug_update():
    finds_list = dir(EntityMedicament)

    medicament_id = request.values.get("medicament_id")
    bar_code = request.values.get("bar_code")
    medicament_obj = BllMedicament().findEntity(
        EntityMedicament.medicament_id == medicament_id)
    if medicament_id:
        obj = BllMedicament().findEntity(EntityMedicament.bar_code == bar_code)
        if obj.medicament_id == medicament_obj.medicament_id:
            for i in finds_list:
                va = request.values.get(i)
                if va:
                    setattr(medicament_obj, i, request.values.get(i))
            BllMedicament().update(medicament_obj)
            return jsonify(Utils.true_return(msg="修改成功"))
        else:
            return jsonify(Utils.true_return(msg="条码不能重复"))
    return jsonify(Utils.true_return(msg="试剂id不能为空"))


# 删除数据
@drug_router.route("/del", methods=["GET", "POST"])
@token_auth.login_required
def drug_del():
    drug_id = request.values.get("medicament_id")
    try:
        BllMedicament().delete(EntityMedicament.medicament_id==drug_id)
        return jsonify(Utils.true_return())
    except Exception:
        return jsonify(Utils.except_return(msg="删除失败"))


# 流转记录
@drug_router.route("/get_durg_record", methods=["GET", "POST"])
@token_auth.login_required
def get_durg_record():
    medicament_id = request.values.get("medicament_id")
    func_type = request.values.get("func_type")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)
    data_list = BllMedicamentRecord().get_drug_flow_log(
        medicament_id=medicament_id,
        func_type=func_type,
        page_param=page_param)
    return jsonify(Utils.true_return(data={"data_list": Utils.msyql_table_model(data_list), "total_count":page_param.totalRecords}))


# 设置空瓶
@drug_router.route("/set_drug_empty_bottle", methods=["GET", "POST"])
@token_auth.login_required
def drug_set_empty_bottle():
    drug_id = request.values.get("medicament_id")
    empty_str = request.values.get("empty_str")
    obj = BllMedicament()
    drug_obj = obj.findEntity(EntityMedicament.medicament_id == drug_id)
    if drug_obj:
        if drug_obj.status == 3:
            return jsonify(Utils.false_return(msg="该试剂为空瓶，无需设置"))
        else:
            drug_obj.status = 3
            if empty_str:
                drug_obj.remark11=empty_str
            obj.update(drug_obj)
            return jsonify(Utils.true_return())
    else:
        return jsonify(Utils.false_return(msg="试剂id有误"))


@drug_router.route("/update_drug_remain", methods=["GET", "POST"])
@token_auth.login_required
def update_drug_remain():
    drug_id = request.values.get("medicament_id")
    remain = request.values.get("remain")
    obj = BllMedicament().findEntity(EntityMedicament.medicament_id==drug_id)
    if obj:
        if not obj.remain:
            setattr(obj, "status", "3")
        else:
            setattr(obj, "remain", remain)
        BllMedicament().update(obj)
        return jsonify(Utils.true_return())
    else:
        return jsonify(Utils.false_return())


#################################
##验证通过后需要添加至试剂领用处。##
#################################
# 分配禁用用户
@drug_router.route("/user_ban_list", methods=["GET", "POST"])
@token_auth.login_required
def drug_ban_user_list():
    seach_user = request.values.get("seach_user")
    drug_id = request.values.get("drug_id")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)
    data_list = BllUserMedicament().get_user_jin_medicament(
        seach_user=seach_user, page_param=page_param,
        drug_id=drug_id
        )
    return jsonify(Utils.true_return(data={"data": Utils.msyql_table_model(data_list), "total_count": page_param.totalRecords}))

# 确认禁用
@drug_router.route("/user_ban_confirm", methods=["GET", "POST"])
@token_auth.login_required
def drug_ban_user_confirm():
    user_id = request.values.get("user_id")
    drug_id = request.values.get("drug_id")
    data = BllUserMedicament().inster_user_drug(user_id, drug_id)
    if data:
        return jsonify(Utils.true_return())
    else:
        return jsonify(Utils.false_return())

# # 获取用户禁用试剂列表
# @drug_router.route("/user_ban_drug_list", methods=["GET", "POST"])
# @token_auth.login_required
# def get_user_ban_drug_list():
#     user_id = request.values.get("user_id")
#     data_list = BllUserMedicament().get_user_drug_name(user_id)
#     return jsonify(Utils.true_return(data=Utils.msyql_table_model(data_list)))

# 取消禁用试剂
@drug_router.route("/user_ban_relieve", methods=["GET", "POST"])
@token_auth.login_required
def user_ban_relieve():
    user_id = request.values.get("user_id")
    drug_id = request.values.get("drug_id")
    data = BllUserMedicament().del_user_ban_drug(user_id=user_id, drug_id=drug_id)
    if data:
        return jsonify(Utils.true_return(msg="删除成功"))
    else:
        return jsonify(Utils.false_return(msg="删除失败"))



# msds数据展示
@drug_router.route("/msds_db_list", methods=["GET", "POST"])
@token_auth.login_required
def get_msds_list():
    seach_word = request.values.get("seach_word")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)
    data_list = BllMsDs().get_seach_list(seach_word=seach_word, page_param=page_param)
    return jsonify(Utils.true_return(data={"data_list": Utils.msyql_table_model(data_list), "total_cout": page_param.totalRecords}))


# 危化品数据展示
@drug_router.route("/dangerous_db_list", methods=["GET", "POST"])
@token_auth.login_required
def get_dangerous_list():
    seach_word = request.values.get("seach_word")
    page = int(request.values.get("page", 1))
    page_size = int(request.values.get("page_size", 10))
    page_param = PageParam(page, page_size)
    data_list = BllDangerous().get_seach_list(seach_word=seach_word, page_param=page_param)
    return jsonify(Utils.true_return(data={"data_list": Utils.msyql_table_model(data_list), "total_cout": page_param.totalRecords}))

# 变更实际类型
@drug_router.route("/updateDrugType", methods=["POST"])
@token_auth.login_required
def DrugTypeUpdate():
    drug_type = request.values.get("drug_type")
    drug_id = request.values.get("drug_id")
    client_id = request.values.get("client_id")
    # client_code = request.values.get("client_code")
    client_code = BllClient().findEntity(EntityClient.client_id==client_id).client_code
    obj = BllMedicament().findEntity(EntityMedicament.medicament_id==drug_id)
    obj.client_id = client_id
    obj.client_code = client_code
    obj.func_type = drug_type

    BllMedicament().update(obj)
    return jsonify(Utils.true_return())




