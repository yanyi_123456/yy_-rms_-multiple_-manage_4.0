#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/08/10 16:53:41
'''
import sys
sys.path.append('.')

from sqlalchemy import and_, or_

from Common.auth import token_auth
from flask import Blueprint, jsonify, request, g
from Common.Utils import PageParam, Utils
from apps.report.utils_base import download_file
from db_logic.humiture_record import BllHumitureRecord

monitor_router = Blueprint('monitor', __name__)



# 每日温度记录
@monitor_router.route("/day_monitor", methods=["GET", "POST"])
# @token_auth.login_required
def get_day_monitor():
    client_id = request.values.get("client_id")
    start_time = request.values.get("start_time")
    end_time = request.values.get("end_time")
    page = request.values.get("page", 1)
    page_size = request.values.get("page_size", 10)
    page_param = PageParam(int(page), int(page_size))

    # 报表数据
    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None

    data_list = BllHumitureRecord().get_client_day_log(
        client_id=client_id, start_time=start_time,
        end_time=end_time, page_param=page_param
        )
    if not download_tp:
        return jsonify(Utils.true_return(data={"data_list": Utils.msyql_table_model(data_list), "total_count": page_param.totalRecords}))
    else:
        file_name = request.values.get("file_name", "数据表")
        rsp = download_file(
            # file_path=file_path,
            file_name=file_name,
            data_list=data_list,
            tp=download_tp
        )
        if rsp:
            return rsp
        else:
            return jsonify(Utils.false_return())

# 获取每日记录详情
@monitor_router.route("/day_monitor_info", methods=["GET", "POST"])
@token_auth.login_required
def get_day_monitor_info():
    client_id = request.values.get("client_id")
    start_time = request.values.get("start_time")
    end_time = request.values.get("end_time")
    page = request.values.get("page", 1)
    page_size = request.values.get("page_size", 10)
    page_param = PageParam(int(page), int(page_size))

    # 报表数据
    download_tp = request.values.get("download_type", 0)
    if download_tp:
        page_param = None
    data_list = BllHumitureRecord().get_client_day_log_info(
        client_id=client_id,
        start_time=start_time,
        end_time=end_time,
        page_param=page_param
    )
    if not download_tp:
        return jsonify(Utils.true_return(data={"data_list": Utils.msyql_table_model(data_list), "total_count": page_param.totalRecords}))
    else:
        file_name = request.values.get("file_name", "数据表")
        rsp = download_file(
            # file_path=file_path,
            file_name=file_name,
            data_list=data_list,
            tp=download_tp
        )
        if rsp:
            return rsp
        else:
            return jsonify(Utils.false_return())
