#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Date:2022/07/18 15:00:54
'''
import sys
sys.path.append(".")
from config.server import create_app
from Common.exception_base import APIException
from config.SystemConfig import SystemConfig
app = create_app()


@app.errorhandler(Exception)
def framework_error(e):
    # 1、APIException
    # 2、Exception
    if isinstance(e, APIException):
        return e
    else:
        if not app.config['DEBUG']:
            return APIException(msg="系统异常错误: {}".format(e))
        else:
            return e



def main():
    HOST = SystemConfig.getConfig("serverip")
    PORT = int(SystemConfig.getConfig("port"))
    # CORS(app)
    # HOST = '127.0.0.1'
    # PORT = 8080
    app.run(host=HOST, port=PORT, debug=True)



if __name__ == '__main__':
    try:
        import sys
        sys.exit(main())
    except Exception as error:
        import traceback
        traceback.print_exc()